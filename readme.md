## Content View

This view is a generic way to display visual content while supporting translation (scrolling), rotation and zoom.

### Installation

1. Add the following in the project's top level `build.gradle` file:

```gradle
allprojects {
    repositories {
        maven {
            url = uri("https://oriient.jfrog.io/artifactory/public-libs-release-local")
        }
    }
}
```

2. Add the following in the app module's `build.gradle` file:

```gradle
dependencies {
    implementation 'me.oriient:content-view:1.1.0'
}
```

### Usage

Every class that extends the [Content](/content-view/src/main/java/me/oriient/ui/contentview/models/Content.kt) abstract class and 
implements the relevant methods can display on the [ContentView](/content-view/src/main/java/me/oriient/ui/contentview/ContentView.kt).
[DrawingOptions](/content-view/src/main/java/me/oriient/ui/contentview/DrawingOptions.kt) are applied to each content on the view.

> Note: you are not required to handle the [DrawingOptions](/content-view/src/main/java/me/oriient/ui/contentview/DrawingOptions.kt), 
> they are handled by the drawing mechanism of the [ContentView](/content-view/src/main/java/me/oriient/ui/contentview/ContentView.kt).
> For example, if `DrawingOptions.resistScale` is set to `true`, 
> the content will always receive a scale factor of 1.

In case the content item requires an image to display, an [ImageProvider](/content-view/src/main/java/me/oriient/ui/contentview/ImageProvider.kt) 
should be implemented and an image identifier should be assigned to the content.
Using a single provider allows for reusing resources that are already loaded by the application 
and maintaining a single application images cache.

This library provides several ready made implementations:

- [ArcContent](/content-view/src/main/java/me/oriient/ui/contentview/content/ArcContent.kt)

- [BillboardContent](/content-view/src/main/java/me/oriient/ui/contentview/content/BillboardContent.kt)

- [CircleContent](/content-view/src/main/java/me/oriient/ui/contentview/content/CircleContent.kt)

- [PathContent](/content-view/src/main/java/me/oriient/ui/contentview/content/PathContent.kt)

- [PolygonContent](/content-view/src/main/java/me/oriient/ui/contentview/content/PolygonContent.kt)

- [TextContent](/content-view/src/main/java/me/oriient/ui/contentview/content/TextContent.kt)

For example, displaying a circle on top of an image would look like this:
```kotlin
val background = ImageContent(
    context,
    bitmap,
    PixelRect(right = bitmap.width.px, bottom = bitmap.height.px),
    isBackground = true
)
val circle = CircleContent(
    context,
    PixelCoordinate(500.px, 500.px),
    250.px,
    ContentColor.Int(Color.BLUE)
) {
    clickable = true
    draggable = true
    zOrder = 1
    resistScale = true
}
content_view.addContent(listOf(
    background,
    circle
))
```

> Note: The coordinate system starts at the top left corner of the view.

> Note: Dragging is only partially supported, still under development.

## How the content is being positioned on the view

![This is how to properly position content on the view](images/coordinates-calculation.png)