rootProject.name="ContentView"
include(":content-view")

apply("$rootDir/versions.gradle.kts")

val kotlinVersion: String by extra

enableFeaturePreview("VERSION_CATALOGS")

dependencyResolutionManagement {
    versionCatalogs {
        create("deps") {
            alias("kotlin.stdlib").to("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
            alias("kotlinx.coroutines.core").to("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")
            alias("androidx.appcompat").to("androidx.appcompat:appcompat:1.2.0")
            alias("androidx.core.ktx").to("androidx.core:core-ktx:1.7.0")
            alias("androidx.lifecycle.viewmodel").to("androidx.lifecycle:lifecycle-viewmodel-ktx:2.4.0")
        }
    }
}

include(":content-view-demo-app")
