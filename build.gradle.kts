// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {

    apply("$rootDir/versions.gradle.kts")

    val kotlinVersion: String by extra

    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }

    dependencies {
        classpath("com.android.tools.build:gradle:7.1.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.31")
        classpath("org.jfrog.buildinfo:build-info-extractor-gradle:4.21.0")
        classpath("org.jetbrains.dokka:dokka-gradle-plugin:1.4.32")

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
