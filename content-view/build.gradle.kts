plugins {
    id("com.android.library")
    id("kotlin-android")
    id("com.jfrog.artifactory")
    id("maven-publish")
    id("org.jetbrains.dokka")
}

apply("$rootDir/build-info.gradle.kts")

val versionName: String by extra
val versionCode: Int by extra
val buildType: String by extra
val isRelease = buildType == "release"

apply("$rootDir/versions.gradle.kts")

val androidMinSdk: Int by extra
val androidBuildToolsVersion: String by extra
val androidCompileSdk: Int by extra
val androidTargetSdk: Int by extra

apply("$rootDir/credentials.gradle.kts")

val artifactoryUrl: String = if (isRelease) {
    project.extra["publicUrl"] as String?
} else {
    project.extra["privateUrl"] as String?
} ?: ""
val repoKey: String = if (isRelease) {
    "public-libs-release-local"
} else {
    "libs-dev-local"
}
val artifactoryUsername: String = if (isRelease) {
    project.extra["publicUsername"] as String?
} else {
    project.extra["privateUsername"] as String?
} ?: ""
val artifactoryPassword: String = if (isRelease) {
    project.extra["publicPassword"] as String?
} else {
    project.extra["privatePassword"] as String?
} ?: ""

android {
    setCompileSdkVersion(androidCompileSdk)
    buildToolsVersion = androidBuildToolsVersion

    defaultConfig {
        minSdk = androidMinSdk
        targetSdk = androidTargetSdk

        buildConfigField("int", "CONTENT_VIEW_VERSION_CODE", "$versionCode")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFile("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")

            buildConfigField("String", "CONTENT_VIEW_VERSION_NAME", "\"$versionName\"")
        }

        create("rc") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")

            buildConfigField("String", "CONTENT_VIEW_VERSION_NAME", "\"$versionName\"")
        }

        getByName("debug") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")

            buildConfigField("String", "CONTENT_VIEW_VERSION_NAME", "\"$versionName-debug\"")
        }
    }
}

tasks {
    val javadocTasks = listOf(
        getByName<org.jetbrains.dokka.gradle.DokkaTask>("dokkaGfm"),
        getByName<org.jetbrains.dokka.gradle.DokkaTask>("dokkaJavadoc")
    )

    javadocTasks.forEach {
        it.outputDirectory.set(File("$buildDir/dokka"))
        it.dokkaSourceSets {
            configureEach {
                includes.from("${rootProject.projectDir}/content-view/src/main/java/me/oriient/ui/contentview/content-view-documentation.md")
            }
        }
    }
}

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("content-view") {
                groupId = "me.oriient"
                artifactId = "content-view"
                version = versionName

                from(components[buildType])
            }
        }
    }

    artifactory {
        setContextUrl(artifactoryUrl)

        publish(delegateClosureOf<org.jfrog.gradle.plugin.artifactory.dsl.PublisherConfig> {
            repository(delegateClosureOf<groovy.lang.GroovyObject> {
                setProperty("repoKey", repoKey)
                setProperty("username", artifactoryUsername)
                setProperty("password", artifactoryPassword)
            })
            defaults(delegateClosureOf<groovy.lang.GroovyObject> {
                invokeMethod("publications", "content-view")
                setProperty("publishPom", true)
                setProperty("publishArtifacts", true)
            })
        })
        resolve(delegateClosureOf<org.jfrog.gradle.plugin.artifactory.dsl.ResolverConfig> {
            setProperty("repoKey", "repo")
        })
    }
}

dependencies {
    implementation(deps.kotlin.stdlib)
    implementation(deps.androidx.appcompat)
    implementation(deps.androidx.core.ktx)
    implementation(deps.kotlinx.coroutines.core)
    implementation(deps.androidx.lifecycle.viewmodel)
}