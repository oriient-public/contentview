package me.oriient.ui.contentview.models

interface ContentViewClickListener {
    fun onClick(pixels: PixelCoordinate, inContent: List<ContentId>)
}

interface ContentViewLongClickListener {
    fun onLongClick(pixels: PixelCoordinate, inContent: List<ContentId>)
}

interface ContentViewInteractionListener {
    fun onTouchEvent(event: TouchEvent)
}

interface ContentViewDragListener {
    fun onDragEvent(event: DragEvent)
}