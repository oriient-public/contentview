package me.oriient.ui.contentview.utils

import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.view.MotionEvent
import androidx.annotation.ColorInt
import me.oriient.ui.contentview.models.*

internal fun <T> List<T>.mutableCopyOf(): MutableList<T> {
    val original = this
    return mutableListOf<T>().apply { addAll(original) }
}

internal fun <T> List<T>.copyOf(): List<T> {
    val original = this
    return mutableListOf<T>().apply { addAll(original) }
}

internal fun MotionEvent.toTouchEvent(): TouchEvent? {
    when (action) {
        MotionEvent.ACTION_DOWN -> return TouchEvent.TouchStarted
        MotionEvent.ACTION_UP -> return TouchEvent.TouchEnded
        MotionEvent.ACTION_CANCEL -> return TouchEvent.TouchEnded
    }

    return null
}

internal fun MotionEvent.toScreenCoordinate(): PixelCoordinate {
    return PixelCoordinate(
        rawX.px,
        rawY.px
    )
}

fun Matrix.mapRect(rect: PixelRect) {
    mapRect(rect.rectF)
    rect.set(rect.rectF)
}

fun Matrix.mapCoordinate(coordinate: PixelCoordinate): PixelCoordinate {

    val applyMatrixParams = FloatArray(2)

    applyMatrixParams[0] = coordinate.x.value
    applyMatrixParams[1] = coordinate.y.value

    mapPoints(applyMatrixParams)

    return PixelCoordinate(
        applyMatrixParams[0].px,
        applyMatrixParams[1].px
    )
}

fun Matrix.mapPixels(x: Pixel, y: Pixel): PixelCoordinate {

    val applyMatrixParams = FloatArray(2)

    applyMatrixParams[0] = x.value
    applyMatrixParams[1] = y.value

    mapPoints(applyMatrixParams)

    return PixelCoordinate(
        applyMatrixParams[0].px,
        applyMatrixParams[1].px
    )
}

internal var Paint.contentColor: ContentColor?
    get() = null
    set(value) { value?.applyToPaint(this) }

val Float.px: Pixel
    get() = Pixel(this)

val Long.px: Pixel
    get() = Pixel(toFloat())

val Int.px: Pixel
    get() = Pixel(toFloat())

val Double.px: Pixel
    get() = Pixel(toFloat())

internal val Float.x: Scale
    get() = Scale(this)

internal val Long.x: Scale
    get() = Scale(toFloat())

internal val Int.x: Scale
    get() = Scale(toFloat())

internal val Double.x: Scale
    get() = Scale(toFloat())

val Float.dg: Degrees
    get() = Degrees(this)

val Long.dg: Degrees
    get() = Degrees(toFloat())

val Int.dg: Degrees
    get() = Degrees(toFloat())

val Double.dg: Degrees
    get() = Degrees(toFloat())

private val tempPathToDetectContains = Path()
private val tempRectToDetectContains = RectF()

internal fun Path.contains(coordinate: PixelCoordinate): Boolean {
    tempPathToDetectContains.reset()
    tempPathToDetectContains.moveTo(coordinate.x.value, coordinate.y.value)
    tempRectToDetectContains.set(coordinate.x.value - 1, coordinate.y.value - 1, coordinate.x.value + 1, coordinate.y.value + 1)
    tempPathToDetectContains.addRect(tempRectToDetectContains, Path.Direction.CW)
    tempPathToDetectContains.op(this, Path.Op.DIFFERENCE)
    return tempPathToDetectContains.isEmpty
}

internal fun Path.bounds(rect: RectF) {
    computeBounds(rect, false)
}

fun coordinate(x: Float, y: Float): PixelCoordinate {
    return PixelCoordinate(x.px, y.px)
}

fun coordinate(x: Int, y: Int): PixelCoordinate {
    return PixelCoordinate(x.px, y.px)
}

fun coordinate(x: Double, y: Double): PixelCoordinate {
    return PixelCoordinate(x.px, y.px)
}

fun color(@ColorInt color: Int): ContentColor {
    return ContentColor.Int(color)
}

val String.contentId: ContentId
    get() { return ContentId(this) }




