package me.oriient.ui.contentview

import me.oriient.ui.contentview.models.PixelMatrix

/**
 * Listener that reacts on scale changes of the map view.
 *
 * @since 1.4.0
 */
interface MapScaleListener {

    /**
     * Method that is called when scale changes.
     *
     * @param matrix current transformation matrix.
     *
     * @since 1.4.0
     */
    fun onScaleChanged(matrix: PixelMatrix)
}