package me.oriient.ui.contentview.utils

import android.util.Log
import android.view.MotionEvent
import java.lang.Exception
import kotlin.math.atan2

private const val TAG = "RotationGestureDetector"
private const val INVALID_POINTER_ID = -1

internal class RotationGestureDetector(private val mListener: OnRotationGestureListener?) {
    private var fX = 0f
    private var fY = 0f
    private var sX = 0f
    private var sY = 0f
    private var focalX = 0f
    private var focalY = 0f
    private var ptrID1 = 0
    private var ptrID2 = 0
    private var mAngle = 0f
    private var firstTouch = false

    fun getAngle(): Float {
        return mAngle
    }

    init {
        ptrID1 = INVALID_POINTER_ID
        ptrID2 = INVALID_POINTER_ID
    }


    fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                sX = event.x
                sY = event.y
                ptrID1 = event.getPointerId(0)
                mAngle = 0f
                firstTouch = true
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                fX = event.x
                fY = event.y
                focalX = getMidpoint(fX, sX)
                focalY = getMidpoint(fY, sY)
                ptrID2 = event.getPointerId(event.actionIndex)
                mAngle = 0f
                firstTouch = true
            }
            MotionEvent.ACTION_MOVE ->

                try {
                    if (ptrID1 != INVALID_POINTER_ID && ptrID2 != INVALID_POINTER_ID) {
                        val nfX: Float = event.getX(event.findPointerIndex(ptrID2))
                        val nfY: Float = event.getY(event.findPointerIndex(ptrID2))
                        val nsX: Float = event.getX(event.findPointerIndex(ptrID1))
                        val nsY: Float = event.getY(event.findPointerIndex(ptrID1))
                        if (firstTouch) {
                            mAngle = 0f
                            firstTouch = false
                        } else {
                            mAngle = angleBetweenLines(fX, fY, sX, sY, nfX, nfY, nsX, nsY)
                        }

                        mListener?.onRotation(getAngle())

                        fX = nfX
                        fY = nfY
                        sX = nsX
                        sY = nsY
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "onTouchEvent: ${e.message}")
                }
            MotionEvent.ACTION_UP -> ptrID1 =
                INVALID_POINTER_ID
            MotionEvent.ACTION_POINTER_UP -> ptrID2 =
                INVALID_POINTER_ID
        }
        return true
    }

    private fun getMidpoint(a: Float, b: Float): Float {
        return (a + b) / 2
    }

    private fun findAngleDelta(angle1: Float, angle2: Float): Float {
        val from = clipAngleTo0To360(angle2)
        val to = clipAngleTo0To360(angle1)

        var dist = to - from

        if (dist < -180.0f) {
            dist += 360.0f
        } else if (dist > 180.0f) {
            dist -= 360.0f
        }

        return dist
    }

    private fun clipAngleTo0To360(Angle: Float): Float {
        return Angle % 360.0f
    }

    private fun angleBetweenLines(
            fx1: Float,
            fy1: Float,
            fx2: Float,
            fy2: Float,
            sx1: Float,
            sy1: Float,
            sx2: Float,
            sy2: Float
    ): Float {
        val angle1 = atan2((fy1 - fy2).toDouble(), (fx1 - fx2).toDouble()).toFloat()
        val angle2 = atan2((sy1 - sy2).toDouble(), (sx1 - sx2).toDouble()).toFloat()

        return findAngleDelta(
                Math.toDegrees(angle1.toDouble()).toFloat(),
                Math.toDegrees(angle2.toDouble()).toFloat()
        )
    }

    internal interface OnRotationGestureListener {
        fun onRotation(angle: Float): Boolean
    }
}