package me.oriient.ui.contentview.content

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.util.Log
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.ImageProvider
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.contentColor
import kotlin.math.hypot

private const val TAG = "CircleContent"

/**
 * A [Content] implementation for drawing a circle on a [me.oriient.ui.contentview.ContentView].
 */
open class CircleContent(
    customContentId: ContentId,
    private val center: PixelCoordinate,
    private val radius: Pixel,
    color: ContentColor,
    strokeWidth: Pixel? = null,
    strokeColor: ContentColor? = null,
    override val anchorPoint: PixelCoordinate = center,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId, null, configBlock) {

    constructor(
        center: PixelCoordinate,
        radius: Pixel,
        color: ContentColor,
        strokeWidth: Pixel? = null,
        strokeColor: ContentColor? = null,
        anchorPoint: PixelCoordinate = center,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, center, radius, color, strokeWidth, strokeColor, anchorPoint, configBlock)

    private val paint = Paint()
    private val strokePaint = Paint()
    private val drawStroke: Boolean

    override val bounds = PixelRect(center.x - radius, center.y - radius, center.x + radius, center.y + radius)

    init {
        paint.contentColor = color
        paint.isAntiAlias = true

        drawStroke = strokeWidth != null && strokeColor != null

        if (drawStroke) {
            strokePaint.style = Paint.Style.STROKE
            strokePaint.contentColor = strokeColor
            strokePaint.strokeWidth = strokeWidth!!.value
        }
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {

        canvas.drawCircle(center.x.value, center.y.value, radius.value, paint)

        if (drawStroke) {
            canvas.drawCircle(center.x.value, center.y.value, radius.value, strokePaint)
        }

    }

    override fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        return hypot(center.x.value - coordinate.x.value, center.y.value - coordinate.y.value) <= radius.value
    }

    override fun move(dx: Pixel, dy: Pixel) {
        center.offset(dx, dy)
        anchorPoint.offset(dx, dy)
        bounds.move(dx, dy)
    }

    override fun toString(): String {
        return "CircleContent(center=$center, radius=$radius, anchorPoint=$anchorPoint, drawStroke=$drawStroke)"
    }
}