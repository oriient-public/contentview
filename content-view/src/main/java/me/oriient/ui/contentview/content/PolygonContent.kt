package me.oriient.ui.contentview.content

import android.graphics.*
import android.util.Log
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.bounds
import me.oriient.ui.contentview.utils.contentColor
import me.oriient.ui.contentview.utils.px


private const val TAG = "PolygonContent"

/**
 * A [Content] implementation for drawing a polygon on a [me.oriient.ui.contentview.ContentView].
 */
// TODO: 30/09/2020 support resist scale and rotation for clicking and dragging
@Suppress("MemberVisibilityCanBePrivate", "CanBeParameter")
open class PolygonContent(
    customContentId: ContentId,
    val coordinates: List<PixelCoordinate>,
    val color: ContentColor,
    val strokeWidth: Pixel? = null,
    val strokeColor: ContentColor? = null,
    val cornerRadius: Pixel = 0.px,
    private val customAnchorPoint: PixelCoordinate? = null,
    val configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId, null, configBlock) {

    constructor(
        coordinates: List<PixelCoordinate>,
        color: ContentColor,
        strokeWidth: Pixel? = null,
        strokeColor: ContentColor? = null,
        cornerRadius: Pixel = 0.px,
        customAnchorPoint: PixelCoordinate? = null,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, coordinates, color, strokeWidth, strokeColor, cornerRadius, customAnchorPoint, configBlock)

    private val paint = Paint()
    private val strokePaint = Paint()
    private val drawStroke: Boolean
    private val path = Path()
    private val pathBounds = RectF()
    private val pathRegion = Region()

    override val anchorPoint: PixelCoordinate
        get() {
            return customAnchorPoint ?: let {
                path.bounds(pathBounds)
                PixelCoordinate(pathBounds.centerX().px, pathBounds.centerY().px)
            }
        }

    override val bounds: PixelRect
        get() {
            return PixelRect(left, top, right, bottom)
        }

    private val top: Pixel
        get() {
            return coordinates.map { it.y }.minOrNull() ?: 0.px
        }

    private val bottom: Pixel
        get() {
            return coordinates.map { it.y }.maxOrNull() ?: 0.px
        }

    private val left: Pixel
        get() {
            return coordinates.map { it.x }.minOrNull() ?: 0.px
        }

    private val right: Pixel
        get() {
            return coordinates.map { it.x }.maxOrNull() ?: 0.px
        }

    init {
        paint.contentColor = color
        paint.isAntiAlias = true
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeCap = Paint.Cap.ROUND
        paint.pathEffect = CornerPathEffect(cornerRadius.value)

        drawStroke = strokeWidth != null && strokeColor != null

        if (drawStroke) {
            strokePaint.style = Paint.Style.STROKE
            strokePaint.contentColor = strokeColor
            strokePaint.strokeWidth = strokeWidth!!.value
            strokePaint.isAntiAlias = true
            strokePaint.strokeJoin = Paint.Join.ROUND
            strokePaint.strokeCap = Paint.Cap.ROUND
            strokePaint.pathEffect = CornerPathEffect(cornerRadius.value)
        }
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {

        if (coordinates.size < 3) {
            Log.e(TAG, "draw: attempting to draw a polygon with less than 3 coordinates!")
            return
        }

        path.reset()
        val firstCoordinate = coordinates[0]
        path.moveTo(firstCoordinate.x.value, firstCoordinate.y.value)
        for (i in 1 until coordinates.size) {
            val coordinate = coordinates[i]
            path.lineTo(coordinate.x.value, coordinate.y.value)
        }
        path.close()

        canvas.drawPath(path, paint)

        if (drawStroke) {
            canvas.drawPath(path, strokePaint)
        }
    }

    override fun move(dx: Pixel, dy: Pixel) {
        coordinates.forEach {
            it.offset(dx, dy)
        }
        anchorPoint.offset(dx, dy)
    }

    override fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        path.bounds(pathBounds)
        return pathBounds.contains(coordinate.x.value, coordinate.y.value)
    }

    override fun toString(): String {
        return "PolygonContent(coordinates=$coordinates, color=$color, strokeWidth=$strokeWidth, strokeColor=$strokeColor, cornerRadius=$cornerRadius, anchorPoint=$anchorPoint, drawStroke=$drawStroke, pathRegion=$pathRegion, bounds=$bounds, top=$top, bottom=$bottom, left=$left, right=$right)"
    }


}