package me.oriient.ui.contentview.models

import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.util.Log
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange
import me.oriient.ui.contentview.DraggedContent
import me.oriient.ui.contentview.utils.px
import java.util.*

/**
 * This is a wrapper class around [android.graphics.RectF] using pixels.
 */
class PixelRect(left: Pixel = 0.px,
                top: Pixel = 0.px,
                right: Pixel = 0.px,
                bottom: Pixel = 0.px
) {

    internal constructor(rect: RectF): this(rect.left.px, rect.top.px, rect.right.px, rect.bottom.px)

    internal val rectF = RectF(left.value, top.value, right.value, bottom.value)

    var left = left
        private set
    var top = top
        private set
    var right = right
        private set
    var bottom = bottom
        private set
    var height = bottom - top
        private set
    var width = right - left
        private set
    val centerX: Pixel
        get() { return left + width / 2 }
    val centerY: Pixel
        get() { return top + height / 2 }
    val center: PixelCoordinate
        get() { return PixelCoordinate(centerX, centerY) }
    val topLeft: PixelCoordinate
        get() { return PixelCoordinate(left, top) }
    val topRight: PixelCoordinate
        get() { return PixelCoordinate(right, top) }
    val bottomLeft: PixelCoordinate
        get() { return PixelCoordinate(left, bottom) }
    val bottomRight: PixelCoordinate
        get() { return PixelCoordinate(right, bottom) }

    init {
        if (height < 0.px) {
            Log.e(TAG, "$TAG must have a positive height")
        }
        if (width < 0.px) {
            Log.e(TAG, "$TAG must have a positive width")
        }
    }

    fun contains(contentCoordinate: PixelCoordinate): Boolean {
        return contentCoordinate.x in left..right && contentCoordinate.y >= top && contentCoordinate.y <= bottom
    }

    fun copy(): PixelRect {
        return PixelRect(left.value.px, top.value.px, right.value.px, bottom.value.px)
    }

    fun set(left: Pixel, top: Pixel, right: Pixel, bottom: Pixel) {
        this.left = left
        this.right = right
        this.top = top
        this.bottom = bottom
        height = bottom - top
        width = right - left
        rectF.set(left.value, top.value, right.value, bottom.value)
    }

    fun set(rect: PixelRect) {
        val copy = rect.copy()
        set(copy.left, copy.top, copy.right, copy.bottom)
    }

    internal fun set(rect: RectF) {
        set(rect.left.px, rect.top.px, rect.right.px, rect.bottom.px)
    }

    internal fun set(rect: Rect) {
        set(rect.left.px, rect.top.px, rect.right.px, rect.bottom.px)
    }

    fun reset() {
        set(0.px, 0.px, 0.px, 0.px)
    }

    fun increaseBy(dx: Pixel, dy: Pixel) {
        set(left - dx, top - dy, right + dx, bottom + dy)
    }

    internal fun scale(scale: Scale) {
        val widthDiff = (width * scale - width) / 2
        val heightDiff = (height * scale - height) / 2
        set(left - widthDiff, top - heightDiff, right + widthDiff, bottom + heightDiff)
    }

    fun inset(pixel: Pixel): PixelRect {
        return PixelRect(
            if (width.value > 2f * pixel.value) left + pixel else width / 2,
            if (height.value > 2f * pixel.value) top + pixel else height / 2,
            if (width.value > 2f * pixel.value) right - pixel else width / 2,
            if (height.value > 2f * pixel.value) bottom - pixel else height / 2
        )
    }

    fun insetInto(pixel: Pixel, rect: PixelRect): PixelRect {
        rect.set(if (width.value > 2f * pixel.value) left + pixel else width / 2,
            if (height.value > 2f * pixel.value) top + pixel else height / 2,
            if (width.value > 2f * pixel.value) right - pixel else width / 2,
            if (height.value > 2f * pixel.value) bottom - pixel else height / 2)
        return rect
    }

    internal fun isOverlapping(other: Rect): Boolean {
        if (right.value < other.left
            || left.value > other.right) {
            return false
        }
        if (top.value < other.bottom
            || bottom.value > other.top) {
            return false
        }
        return true
    }

    fun intersect(other: PixelRect): Boolean {
        return left < other.right &&
            right > other.left &&
            top < other.bottom &&
            bottom > other.top
    }

    fun isOverlapping(other: PixelRect): Boolean {
        if (right < other.left
            || left > other.right) {
            return false
        }
        if (top < other.bottom
            || bottom > other.top) {
            return false
        }
        return true
    }

    fun move(dx: Pixel, dy: Pixel) {
        left += dx
        right += dx
        top += dy
        bottom += dy
        rectF.set(left.value, top.value, right.value, bottom.value)
    }

    fun contains(rect: PixelRect): Boolean {
        return left < rect.left && right > rect.right && top < rect.top && bottom > rect.bottom
    }

    fun diff(rect: PixelRect): PixelRect {
        return PixelRect(rect.left - left, rect.top - top, right - rect.right, bottom - rect.bottom)
    }

    operator fun times(other: Pixel): PixelRect {
        return PixelRect(
            left * other,
            top * other,
            right * other,
            bottom * other,
        )
    }

    override fun toString(): String {
        return "[left=${left.value}, top=${top.value}, right=${right.value}, bottom=${bottom.value}], [height=${height.value}, width=${width.value})]"
    }

    companion object {
        private const val TAG = "PixelRect"
    }
}

/**
 * This class represents a [x, y] coordinate of pixels.
 */
class PixelCoordinate(x: Pixel, y: Pixel) {

    var x = x
        private set
    var y = y
        private set

    constructor(): this(0.px, 0.px)

    fun offset(ofX: Pixel, ofY: Pixel) {
        x += ofX
        y += ofY
    }

    fun updateX(x: Pixel) {
        this.x = x
    }

    fun updateY(y: Pixel) {
        this.y = y
    }

    operator fun times(other: Float): PixelCoordinate {
        return PixelCoordinate(x * other, y * other)
    }

    fun copy(): PixelCoordinate {
        return PixelCoordinate(x, y)
    }

    override fun toString(): String {
        return "${javaClass.simpleName}(x=$x, y=$y)"
    }
}

/**
 * This class represents relative anchor point
 * @property x x value in the interval [0, 1]
 * @property y y value in the interval [0, 1]
 *
 * @since 1.4.0
 */
data class AnchorPoint(
    @FloatRange(from = 0.0, to = 1.0)
    val x: Float,

    @FloatRange(from = 0.0, to = 1.0)
    val y: Float
)

/**
 * This class represents a unique identifier of [Content] classes.
 */
/*value*/ class ContentId(val value: String): Comparable<ContentId> {

    companion object {
        val random: ContentId
            get() { return ContentId(UUID.randomUUID().toString()) }
    }

    override fun compareTo(other: ContentId): Int {
        return value.compareTo(other.value)
    }

    override fun toString(): String {
        return "ContentId(value='$value')"
    }

    override fun equals(other: Any?): Boolean {
        if (other is ContentId) {
            return other.value == value
        }
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }
}

/**
 * An event of the view being touched.
 */
sealed class TouchEvent {
    object TouchStarted: TouchEvent()
    object TouchEnded: TouchEvent()
}

/**
 * Holds different representations of color to be used by content.
 */
abstract class ContentColor {
    abstract fun applyToPaint(paint: Paint)
    @ColorInt
    abstract fun toAndroidColorInt(): kotlin.Int

    class Int(@ColorInt private val color: kotlin.Int): ContentColor() {
        override fun applyToPaint(paint: Paint) {
            paint.color = color
        }

        override fun toAndroidColorInt(): kotlin.Int {
            return color
        }
    }

    class Shader(private val shader: android.graphics.Shader): ContentColor() {
        override fun applyToPaint(paint: Paint) {
            paint.shader = shader
        }

        override fun toAndroidColorInt(): kotlin.Int {
            // TODO: 21/09/2020 implement
            return Color.WHITE
        }
    }
}

/**
 * Events as a result of drag actions by the user.
 */
sealed class DragEvent(val content: Content, val position: PixelCoordinate) {

    class DragStarted(content: Content, position: PixelCoordinate): DragEvent(content, position) {
        internal constructor(draggedContent: DraggedContent): this(draggedContent.content, draggedContent.position)
    }
    class DragEnded(content: Content, position: PixelCoordinate): DragEvent(content, position) {
        internal constructor(draggedContent: DraggedContent): this(draggedContent.content, draggedContent.position)
    }
    class DragCanceled(content: Content, position: PixelCoordinate): DragEvent(content, position) {
        internal constructor(draggedContent: DraggedContent): this(draggedContent.content, draggedContent.position)
    }

    override fun toString(): String {
        return "${javaClass.simpleName}{$content},[$position]"
    }
}

/**
 * Parameters to display billboards.
 */
class BillboardParameters(
    // actual billboard rectangle
    val width: Pixel = 100.px,
    val height: Pixel = 75.px,
    val localAnchorPoint: AnchorPoint = AnchorPoint(0.5f, 1.0f),

    val bottomStandHeight: Pixel = 12.px,
    val bottomStandWidth: Pixel = 24.px,
    // margin of image on billboard rectangle
    val imageMargins: Pixel = 5.px,
    val color: ContentColor = ContentColor.Int(Color.WHITE),
)

/**
 * Rectangular size in pixels.
 */
class PixelSize(val width: Pixel, val height: Pixel)