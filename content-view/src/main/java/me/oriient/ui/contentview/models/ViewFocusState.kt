package me.oriient.ui.contentview.models

internal data class ViewFocusState(
    val scale: Scale,
    val rotation: Degrees,
    val center: PixelCoordinate,
    val width: Pixel,
    val height: Pixel,
    val offset: PixelCoordinate,
)
