package me.oriient.ui.contentview.models

import android.graphics.Bitmap
import android.graphics.Canvas
import me.oriient.ui.contentview.ContentView
import me.oriient.ui.contentview.DrawingOptions
import me.oriient.ui.contentview.DrawingOptionsImpl
import java.util.*

/**
 * Abstract class that should be extended by every content to be displayed on the [ContentView].
 */

@Suppress("unused")
abstract class Content constructor(
    /**
     * Unique identifier of this content.
     */
    val contentId: ContentId,
    val imageIdentifier: String?,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
) : Comparable<Content?> {

    constructor(imageIdentifier: String?, configBlock: DrawingOptionsImpl.() -> Unit = {}): this(ContentId.random, imageIdentifier, configBlock)

    constructor(configBlock: DrawingOptionsImpl.() -> Unit = {}): this(null, configBlock)

    @Suppress("PropertyName")
    internal var _anchorPoint = PixelCoordinate()
    /**
     * This method provides the point around which this content will be rotated in case [DrawingOptions.resistRotation] is set to `true`.
     */
    open val anchorPoint: PixelCoordinate
        get() { return _anchorPoint }

    internal var _options = DrawingOptionsImpl()
    /**
     * The contents drawing options.
     */
    val options: DrawingOptions
        get() { return _options }

    internal val matrix = PixelMatrix()

    internal var _bounds: PixelRect? = null
    /**
     * A rectangular representation of the area which this content occupies.
     * Dragging will limit this area to remain inside the background bounds.
     */
    open val bounds: PixelRect?
        get() { return _bounds }

    init {
        _options.apply(configBlock)
    }

    /**
     * Apply new drawing options. May be partial.
     */
    fun applyOptions(configBlock: DrawingOptionsImpl.() -> Unit = {}) {
        _options.apply(configBlock)
    }

    /**
     * Set new drawing options.
     */
    fun setOptions(options: DrawingOptions) {
        this._options = DrawingOptionsImpl(options)
    }

    /**
     * This method must draw the content on the canvas.
     */
    abstract fun draw(
        canvas: Canvas,
        transformMatrix: PixelMatrix,
        image: Bitmap?,
    )

    /**
     * This method must return true if the coordinate is inside the click area of the content.
     * [DrawingOptions.clickable] will not be supported if this method is not overridden.
     */
    open fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        return false
    }

    /**
     * This method should move the location of the content.
     * [DrawingOptions.draggable] will not be supported if this method is not overridden.
     */
    open fun move(dx: Pixel, dy: Pixel) {  }

    /**
     * Compares display order of content.
     * DO NOT OVERRIDE THIS METHOD!
     */
    override fun compareTo(other: Content?): Int {
        if (other == null) {
            return 1
        }
        return if (options.zOrder != other.options.zOrder) {
            options.zOrder.compareTo(other.options.zOrder)
        } else {
            options.zPriority.compareTo(other.options.zPriority)
        }
    }
}
