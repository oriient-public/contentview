package me.oriient.ui.contentview

import android.graphics.Matrix
import me.oriient.ui.contentview.models.Degrees
import me.oriient.ui.contentview.models.Pixel
import me.oriient.ui.contentview.models.PixelMatrix
import me.oriient.ui.contentview.models.Scale
import me.oriient.ui.contentview.utils.px
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt


private const val TAG = "DisplayMetrics"

internal class DisplayMetricsGenerator {

    val matrix = PixelMatrix()
    val inverseMatrix = PixelMatrix()
    private val matrixValues = FloatArray(9)

    val scaleFactor: Scale
        get() {
            return Scale(
                sqrt(
                    matrixValues[Matrix.MSCALE_X].pow(2) +
                            matrixValues[Matrix.MSKEW_Y].pow(2)
                )
            )
        }

    val rotationDegrees: Degrees
        get() {
            return Degrees(
                atan2(
                    matrixValues[Matrix.MSKEW_X],
                    matrixValues[Matrix.MSCALE_X]
                ) * (180f / Math.PI.toFloat())
            )
        }

    var focusX = 0.px
        internal set
    var focusY = 0.px
        internal set
    val translationX: Pixel
        get() { return matrixValues[Matrix.MTRANS_X].px }
    val translationY: Pixel
        get() { return matrixValues[Matrix.MTRANS_Y].px }

    var isBeingTouched = false
        internal set

    fun updateMatrix(newMatrix: PixelMatrix) {
        matrix.set(newMatrix)
        matrix.copyInto(matrixValues)
        matrix.invert(inverseMatrix)
    }

    override fun toString(): String {
        return "DisplayMetrics(matrix=$matrix, matrixValues=${matrixValues.contentToString()}, focusX=$focusX, focusY=$focusY, isBeingTouched=$isBeingTouched)"
    }
}

private val matrixValues = FloatArray(9)

internal val Matrix.scaleFactor: Scale
    @Synchronized
    get() {
        getValues(matrixValues)
        return Scale(
            sqrt(
                matrixValues[Matrix.MSCALE_X].pow(2) +
                        matrixValues[Matrix.MSKEW_Y].pow(2)
            )
        )
    }

internal val Matrix.rotationDegrees: Degrees
    @Synchronized
    get() {
        getValues(matrixValues)
        return Degrees(
            atan2(
                matrixValues[Matrix.MSKEW_X],
                matrixValues[Matrix.MSCALE_X]
            ) * (180f / Math.PI.toFloat())
        )
    }

internal val Matrix.translationX: Pixel
    @Synchronized
    get() {
        getValues(matrixValues)
        return matrixValues[Matrix.MTRANS_X].px
    }

internal val Matrix.translationY: Pixel
    @Synchronized
    get() {
        getValues(matrixValues)
        return matrixValues[Matrix.MTRANS_Y].px
    }