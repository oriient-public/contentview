package me.oriient.ui.contentview

import android.content.Context
import me.oriient.ui.contentview.models.PixelCoordinate

/**
 * Implement this interface to provide images for content elements.
 * Using a single provider allows for reusing resources that are already loaded by the application
 * and maintaining a single application images cache.
 * Assign image identifiers to content items that require images to be displayed.
 */
interface ImageProvider {
    /**
     * This method will be called every time the [ContentView] wants to draw a content item with a
     * non-null image identifier.
     */
    fun provideImage(context: Context, imageIdentifier: String): ImageRequest?
}

/**
 * Every request to the [ImageProvider] should return one of the following responses:
 *  - In case the resource is available immediately, a [ImageRequest.Bitmap] should be returned.
 *  - In case the resource is not available immediately, a [ImageRequest.Deferred] should be returned
 *    providing a loader suspending callback. If the loader returns `true`, the [ImageProvider] will
 *    refresh and request all the images once more.
 */
sealed class ImageRequest {
    /**
     * A way to return an image synchronously right away.
     */
    data class Bitmap(val value: android.graphics.Bitmap, val anchorPoint: PixelCoordinate? = null): ImageRequest()

    /**
     * A way to defer returning the image to some time in the future.
     */
    data class Deferred(val loader: suspend () -> Boolean): ImageRequest()

    override fun toString(): String {
        return "$this"
    }
}