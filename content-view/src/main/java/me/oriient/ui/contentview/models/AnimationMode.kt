package me.oriient.ui.contentview.models

enum class AnimationMode {
    None, Linear, AccelerateDecelerate
}