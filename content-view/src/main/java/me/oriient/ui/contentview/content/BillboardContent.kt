package me.oriient.ui.contentview.content

import android.graphics.*
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.bounds
import me.oriient.ui.contentview.utils.px

private const val TAG = "BillboardContent"

/**
 * A [Content] implementation for drawing a billboard on a [me.oriient.ui.contentview.ContentView].
 * The billboard may display an image.
 */
open class BillboardContent(
    customContentId: ContentId,
    private val coordinate: PixelCoordinate,
    imageIdentifier: String?,
    private val imageOverlay: Bitmap? = null,
    private val billboardParameters: BillboardParameters = BillboardParameters(),
    configBlock: DrawingOptionsImpl.() -> Unit = {}
) : Content(customContentId, imageIdentifier, configBlock) {

    constructor(
        coordinate: PixelCoordinate,
        imageIdentifier: String?,
        imageOverlay: Bitmap? = null,
        billboardParameters: BillboardParameters = BillboardParameters(),
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ) : this(ContentId.random, coordinate, imageIdentifier, imageOverlay, billboardParameters, configBlock)

    override val anchorPoint: PixelCoordinate
        get() = coordinate

    private val paint = Paint()
    private val standPaint = Paint()
    private val standShadowPaint = Paint()
    private val shadowCirclePaint = Paint()
    private val shadowCircleSize = PixelSize(18.px, 5.px)
    private val shadowColor = Color.parseColor("#80424242")
    private val path = Path()
    private val standPath = Path()
    private val imageRect = RectF()
    private val clickRect = RectF()
    private val _topLeft = PixelCoordinate()
    private val _bottomRight = PixelCoordinate()

    init {
        billboardParameters.color.applyToPaint(paint)
        paint.isDither = true
        paint.pathEffect = CornerPathEffect(10f)
        paint.isAntiAlias = true
        paint.setShadowLayer(5f, 2f, 2f, shadowColor)

        billboardParameters.color.applyToPaint(standPaint)
        standPaint.isAntiAlias = true

        standShadowPaint.color = Color.TRANSPARENT
        standShadowPaint.isAntiAlias = true
        standShadowPaint.setShadowLayer(2f, 2f, 2f, shadowColor)

        shadowCirclePaint.color = Color.TRANSPARENT
        shadowCirclePaint.isAntiAlias = true
        shadowCirclePaint.setShadowLayer(2f, 0f, 0f, shadowColor)
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {
        standPath.reset()
        standPath.moveTo(coordinate.x.value, coordinate.y.value)
        standPath.lineTo(
            coordinate.x.value - billboardParameters.bottomStandWidth.value / 2f,
            coordinate.y.value - billboardParameters.bottomStandHeight.value - 1f
        )
        standPath.lineTo(
            coordinate.x.value + billboardParameters.bottomStandWidth.value / 2f,
            coordinate.y.value - billboardParameters.bottomStandHeight.value - 1f
        )
        standPath.close()

        path.reset()
        var moveX = 0f
        var moveY = 0f
        val startingX = coordinate.x.value - billboardParameters.width.value / 2f
        val startingY = coordinate.y.value - billboardParameters.bottomStandHeight.value
        path.moveTo(startingX, startingY)
        moveY -= billboardParameters.height.value
        path.lineTo(startingX + moveX, startingY + moveY)
        moveX += billboardParameters.width.value
        path.lineTo(startingX + moveX, startingY + moveY)
        moveY += billboardParameters.height.value
        path.lineTo(startingX + moveX, startingY + moveY)
        path.close()

        canvas.drawOval(
            coordinate.x.value - shadowCircleSize.width.value / 2f,
            coordinate.y.value - shadowCircleSize.height.value / 2f,
            coordinate.x.value + shadowCircleSize.width.value / 2f,
            coordinate.y.value + shadowCircleSize.height.value / 2f,
            shadowCirclePaint
        )
        canvas.drawPath(standPath, standShadowPaint)
        canvas.drawPath(path, paint)
        canvas.drawPath(standPath, standPaint)

        image?.run {

            val imageRatio = height.toFloat() / width.toFloat()
            val billboardRatio = billboardParameters.height.value / billboardParameters.width.value
            val isHorizontal = imageRatio < billboardRatio

            if (isHorizontal) {
                val left = coordinate.x.value - billboardParameters.width.value / 2f + billboardParameters.imageMargins.value
                val right = coordinate.x.value + billboardParameters.width.value / 2f - billboardParameters.imageMargins.value
                val imageHeight = (right - left) * imageRatio
                val bottom = coordinate.y.value - billboardParameters.bottomStandHeight.value - billboardParameters.height.value / 2f + imageHeight / 2f
                val top = coordinate.y.value - billboardParameters.bottomStandHeight.value - billboardParameters.height.value / 2f - imageHeight / 2f
                imageRect.set(left, top, right, bottom)
            } else {
                val bottom = coordinate.y.value - billboardParameters.bottomStandHeight.value - billboardParameters.imageMargins.value
                val top = coordinate.y.value - billboardParameters.bottomStandHeight.value - billboardParameters.height.value + billboardParameters.imageMargins.value
                val imageWidth = (bottom - top) / imageRatio
                val left = coordinate.x.value - imageWidth / 2f
                val right = coordinate.x.value + imageWidth / 2f
                imageRect.set(left, top, right, bottom)
            }

            canvas.drawBitmap(this, null, imageRect, null)

            imageOverlay?.run {

                // TODO: 24/09/2020 optimize performance

                val output = Bitmap.createBitmap(
                    billboardParameters.width.value.toInt(),
                    billboardParameters.height.value.toInt(),
                    Bitmap.Config.ARGB_8888
                )
                val canvas1 = Canvas(output)

                val paint = Paint()
                val rect = Rect(0, 0, billboardParameters.width.value.toInt(), billboardParameters.height.value.toInt())
                val rectF = RectF(rect)
                val roundPx = 10f

                paint.isAntiAlias = true
                canvas1.drawARGB(0, 0, 0, 0)
                canvas1.drawRoundRect(rectF, roundPx, roundPx, paint)

                paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
                canvas1.drawBitmap(this, rect, rect, paint)

                canvas.drawBitmap(output, startingX, startingY - billboardParameters.height.value.toInt(), Paint())
            }

        }
    }

    override fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        path.bounds(clickRect)
        return clickRect.contains(coordinate.x.value, coordinate.y.value)
    }

    override fun move(dx: Pixel, dy: Pixel) {
        coordinate.offset(dx, dy)
    }

    override fun toString(): String {
        return "BillboardContent(coordinate=$coordinate, imageOverlay=$imageOverlay, billboardParameters=$billboardParameters, paint=$paint, standPaint=$standPaint, standShadowPaint=$standShadowPaint, shadowCirclePaint=$shadowCirclePaint, shadowCircleSize=$shadowCircleSize, shadowColor=$shadowColor, path=$path, standPath=$standPath, imageRect=$imageRect, clickRect=$clickRect, _topLeft=$_topLeft, _bottomRight=$_bottomRight)"
    }
}