package me.oriient.ui.contentview.clustering

import me.oriient.ui.contentview.models.PixelCoordinate
import me.oriient.ui.contentview.models.PixelSize

/**
 * An interface the defines mandatory fields for clusterable objects.
 *
 * @since 1.4.0
 */
interface Clusterable {
    /**
     * Coordinates of the anchor point
     *
     * @since 1.4.0
     */
    val pixelCoordinate: PixelCoordinate

    /**
     * Size of the regular item in pixels
     *
     * @since 2.0.0
     */
    val itemSize: PixelSize

    /**
     * Size of the cluster item in pixels
     *
     * @since 2.0.0
     */
    val clusterSize: PixelSize
}