package me.oriient.ui.contentview.clustering

import me.oriient.ui.contentview.ContentView
import me.oriient.ui.contentview.MapScaleListener
import me.oriient.ui.contentview.models.Content
import me.oriient.ui.contentview.models.ContentId
import me.oriient.ui.contentview.models.PixelMatrix
import kotlin.math.abs

/**
 * Generic class that manages clustering algorithm and adding/removing items from the map
 *
 * @since 1.4.0
 */
interface ClusterManager<T : Clusterable> {

    /**
     * Attaches view to cluster manager to give an ability to add and remove clusters to/from the map
     *
     * @since 1.4.0
     */
    fun attachMapView(mapView: ContentView)

    /**
     * Sets items to be clustered
     *
     * @since 1.4.0
     */
    fun setItems(items: List<T>)

    /**
     * Detaches view from cluster manager, removes items from the map, clears reference to view.
     *
     * @since 1.4.0
     */
    fun detachMapView()

    /**
     * Returns the list of items that are contained within a cluster
     * if contentIds are parts of cluster.
     *
     * @param contentIds a list of contentIds representing the items within the cluster
     * @return a list of items that are contained within the cluster
     * or null if no items were found/contentIds are not part of any cluster
     */
    fun findItemsInsideCluster(contentIds: List<ContentId>): List<T>?
}

/**
 * @param minScaleChangeForRecalculation minimal scale change for item recalculation
 * @param minRecalculationTimeIntervalMillis minimal time interval in millis for item recalculation
 * @param clusteringStrategy class that defines how items should be clustered
 * @param singleItemProvider Function that creates Content for a single cluster
 * @param clusterItemProvider Function that creates Content for a cluster consisting of two or more items
 *
 * @since 1.4.0
 */
class ClusterManagerImpl<T : Clusterable>(
    // this value allows to be sensitive enough to react on scale changes,
    // but not recalculate clusters when map rotates.
    private val minScaleChangeForRecalculation: Double = 0.001,
    private val minRecalculationTimeIntervalMillis: Long = 100L,
    private val clusteringStrategy: ClusteringStrategy<T>,
    private val singleItemProvider: (T) -> List<Content>,
    private val clusterItemProvider: (List<T>) -> List<Content>
) : ClusterManager<T> {

    private var items = listOf<T>()
    private var currentClusters = mutableMapOf<T, List<T>>()
    private var clusterContentIds = mutableMapOf<List<T>, List<ContentId>>()

    private var lastScaleUsedForRecalculation: Float? = null
    private var lastInteractionTimeMillis: Long = 0

    private var mapView: ContentView? = null

    private val scaleListener: MapScaleListener = MapScaleListenerImpl()

    override fun attachMapView(mapView: ContentView) {
        this.mapView = mapView
        mapView.addScaleListener(scaleListener)
        evaluateState(mapView.currentTransformations)
    }

    override fun setItems(items: List<T>) {
        this.items = items
        val mapView = mapView ?: return
        evaluateState(mapView.currentTransformations)
    }

    override fun detachMapView() {
        clusterContentIds.values.forEach { items ->
            mapView?.removeContent(items)
        }

        currentClusters.clear()
        clusterContentIds.clear()
        mapView?.removeScaleListener(scaleListener)
        mapView = null
    }

    override fun findItemsInsideCluster(contentIds: List<ContentId>): List<T>? {
        val itemsInside = clusterContentIds
            .filter { entry -> entry.value.any { contentIds.contains(it) } }
            .keys
            .firstOrNull()

        // 1 item doesn't count because it's not a cluster
        return if (itemsInside?.size == 1) {
            null
        } else {
            itemsInside
        }
    }

    private fun evaluateState(matrix: PixelMatrix) {
        val mapView = mapView ?: return
        lastInteractionTimeMillis = System.currentTimeMillis()
        lastScaleUsedForRecalculation = mapView.currentTransformations.scaleFactor.value

        val clustersToAdd = mutableListOf<List<T>>()
        val clustersToRemove = mutableListOf<List<T>>()

        val newClusters = createClustersMap(clusteringStrategy.cluster(items, matrix))
        findNewClusters(
            currentClusters,
            newClusters,
            clustersToAdd,
            clustersToRemove
        )

        findDisappearedClusters(
            currentClusters,
            newClusters,
            clustersToRemove
        )

        removeClustersFromMap(clustersToRemove)
        addClustersToMap(clustersToAdd)

        currentClusters = newClusters
    }

    private fun createClustersMap(
        clusters: List<List<T>>
    ): MutableMap<T, List<T>> {
        val clustersMap = mutableMapOf<T, List<T>>()
        if (clusters.isEmpty()) {
            return clustersMap
        }

        clusters.forEach { items ->
            val firstItem = items.firstOrNull() ?: return@forEach
            clustersMap[firstItem] = items
        }

        return clustersMap
    }

    private fun findNewClusters(
        currentClusters: Map<T, List<T>>,
        newClusters: Map<T, List<T>>,
        clustersToAdd: MutableList<List<T>>,
        clustersToRemove: MutableList<List<T>>
    ) {
        newClusters.forEach { entry ->
            val (newCluster, newItems) = entry
            val oldItems = currentClusters[newCluster]
            if (oldItems == null) {
                // Add cluster if there's no existing cluster like this
                clustersToAdd.add(newItems)
            } else {
                // Item that used to be in a cluster, but should not be deleted and redrawn anymore.
                if (newItems != oldItems) {
                    clustersToRemove.add(oldItems)
                    clustersToAdd.add(newItems)
                }
            }
        }
    }

    private fun findDisappearedClusters(
        currentClusters: Map<T, List<T>>,
        newClusters: Map<T, List<T>>,
        clustersToRemove: MutableList<List<T>>
    ) {
        // find clusters that don't exist in a new cluster
        currentClusters.forEach { entry ->
            val (currentCluster, currentItems) = entry
            if (!newClusters.containsKey(currentCluster)) {
                clustersToRemove.add(currentItems)
            }
        }
    }

    private fun removeClustersFromMap(clusters: List<List<T>>) {
        clusters.forEach { cluster ->
            if (cluster.isEmpty()) {
                return@forEach
            }

            clusterContentIds[cluster]?.let {
                mapView?.removeContent(it)
                clusterContentIds.remove(cluster)
            }
        }
    }

    private fun addClustersToMap(clusters: List<List<T>>) {
        clusters.forEach { cluster ->
            if (cluster.isEmpty()) {
                return@forEach
            }

            val contentToAdd = provideContent(cluster)
            clusterContentIds[cluster] = contentToAdd.map { it.contentId }
            mapView?.addContent(contentToAdd)
        }
    }

    private fun provideContent(cluster: List<T>): List<Content> {
        return if (cluster.size == 1) {
            singleItemProvider.invoke(cluster.first())
        } else {
            clusterItemProvider.invoke(cluster)
        }
    }

    private inner class MapScaleListenerImpl : MapScaleListener {
        override fun onScaleChanged(matrix: PixelMatrix) {
            val mapView = mapView ?: return

            if (System.currentTimeMillis() - lastInteractionTimeMillis < minRecalculationTimeIntervalMillis) {
                return
            }

            val lastUsedScale = lastScaleUsedForRecalculation ?: return
            val currentScale = mapView.currentTransformations.scaleFactor.value
            if (abs(currentScale - lastUsedScale) >= minScaleChangeForRecalculation) {
                evaluateState(matrix)
            }
        }
    }

    companion object {
        private const val TAG = "ClusterManager"
    }

}