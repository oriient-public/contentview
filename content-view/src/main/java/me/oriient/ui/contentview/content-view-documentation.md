# Module content-view

This view is a generic way to display visual content while supporting translation (scrolling), rotation and zoom.

Every class which extends the [Content](me.oriient.ui.contentview.models.Content) abstract class and implements the relevant methods can display on the [ContentView](me.oriient.ui.contentview.ContentView).
[DrawingOptions](me.oriient.ui.contentview.DrawingOptions) are applied to each content on the view.

> :Note: you are not required to handle the [DrawingOptions](me.oriient.ui.contentview.DrawingOptions), they are handled by the drawing mechanism of the [ContentView](me.oriient.ui.contentview.ContentView).
> For example, if [DrawingOptions.resistScale](me.oriient.ui.contentview.DrawingOptions.resistScale) is set to `true`, the content will always receive a scale factor of 1.

This library provides several ready made implementations:

- [ArcContent](me.oriient.ui.contentview.content.ArcContent)

- [BillboardContent](me.oriient.ui.contentview.content.BillboardContent)

- [CircleContent](me.oriient.ui.contentview.content.CircleContent)

- [PathContent](me.oriient.ui.contentview.content.PathContent)

- [PolygonContent](me.oriient.ui.contentview.content.PolygonContent)

- [TextContent](me.oriient.ui.contentview.content.TextContent)

For example, displaying a circle on top of an image would look like this:
```kotlin
val background = ImageContent(
    context,
    bitmap,
    PixelRect(right = bitmap.width.px, bottom = bitmap.height.px),
    isBackground = true
)
val circle = CircleContent(
    context,
    PixelCoordinate(500.px, 500.px),
    250.px,
    ContentColor.Int(Color.BLUE)
) {
    clickable = true
    draggable = true
    zOrder = 1
    resistScale = true
}
content_view.addContent(listOf(
    background,
    circle
))
```

The coordinate system starts at the top left corner of the view.