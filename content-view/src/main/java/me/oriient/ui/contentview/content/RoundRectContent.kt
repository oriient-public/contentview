package me.oriient.ui.contentview.content

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.ImageProvider
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.contentColor
import java.util.*


@Deprecated("Please use PolygonContent with corner radius instead")
open class RoundRectContent(
    customContentId: ContentId,
    override val bounds: PixelRect,
    private val cornerRadius: Pixel,
    color: ContentColor,
    strokeWidth: Pixel? = null,
    strokeColor: ContentColor? = null,
    override val anchorPoint: PixelCoordinate = PixelCoordinate(bounds.centerX, bounds.centerY),
    configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId, null, configBlock) {

    constructor(
        bounds: PixelRect,
        cornerRadius: Pixel,
        color: ContentColor,
        strokeWidth: Pixel? = null,
        strokeColor: ContentColor? = null,
        anchorPoint: PixelCoordinate = PixelCoordinate(bounds.centerX, bounds.centerY),
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, bounds, cornerRadius, color, strokeWidth, strokeColor, anchorPoint, configBlock)

    private val paint = Paint()
    private val strokePaint = Paint()
    private val drawStroke: Boolean

    init {
        paint.contentColor = color
        paint.isAntiAlias = true

        drawStroke = strokeWidth != null && strokeColor != null

        if (drawStroke) {
            strokePaint.style = Paint.Style.STROKE
            strokePaint.contentColor = strokeColor
            strokePaint.strokeWidth = strokeWidth!!.value
        }
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {
        canvas.drawRoundRect(bounds.rectF, cornerRadius.value, cornerRadius.value, paint)

        if (drawStroke) {
            canvas.drawRoundRect(bounds.rectF, cornerRadius.value, cornerRadius.value, paint)
        }
    }
}