package me.oriient.ui.contentview.models

import android.graphics.Canvas
import android.graphics.Matrix
import me.oriient.ui.contentview.utils.px
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * This is a wrapper class around [android.graphics.Matrix] using pixels.
 */
class PixelMatrix(
    private val matrix: Matrix,
    isInitialized: Boolean = false
) {
    private val matrixValues = FloatArray(9)
    internal lateinit var scaleFactor: Scale
        private set
    lateinit var rotationDegrees: Degrees
        private set
    lateinit var translation: PixelCoordinate
        private set

    var isInitialized: Boolean = isInitialized
        private set

    internal constructor(): this(Matrix())

    constructor(pixelMatrix: PixelMatrix): this(Matrix(pixelMatrix.matrix), pixelMatrix.isInitialized)

    init {
        updateMatrixParams()
    }

    fun copy(): PixelMatrix {
        return PixelMatrix(this)
    }

    internal fun set(matrix: Matrix) {
        this.matrix.set(matrix)
        isInitialized = true
        updateMatrixParams()
    }

    fun set(pixelMatrix: PixelMatrix) {
        this.set(pixelMatrix.matrix)
    }

    fun reset() {
        this.matrix.reset()
        isInitialized = false
        updateMatrixParams()
    }

    fun setValues(values: FloatArray) {
        this.matrix.setValues(values)
        isInitialized = true
        updateMatrixParams()
    }

    fun copyInto(values: FloatArray) {
        matrix.getValues(values)
    }

    internal fun apply(rotation: Degrees, scale: Scale, translationX: Pixel, translationY: Pixel) {
        this.apply {
            reset()
            setRotate(-rotation)
            postScale(scale)
            postTranslate(translationX, translationY)
        }
    }

    fun invert(pixelMatrix: PixelMatrix) {
        matrix.invert(pixelMatrix.matrix)
        isInitialized = true
    }

    fun postTranslate(dx: Pixel, dy: Pixel) {
        matrix.postTranslate(dx.value, dy.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun preTranslate(dx: Pixel, dy: Pixel) {
        matrix.preTranslate(dx.value, dy.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun setTranslate(dx: Pixel, dy: Pixel) {
        matrix.setTranslate(dx.value, dy.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun postScale(sx: Scale, sy: Scale) {
        matrix.postScale(sx.value, sy.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun preScale(sx: Scale, sy: Scale) {
        matrix.preScale(sx.value, sy.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun setScale(sx: Scale, sy: Scale) {
        matrix.setScale(sx.value, sy.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun postScale(sx: Scale, sy: Scale, px: Pixel, py: Pixel) {
        matrix.postScale(sx.value, sy.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun preScale(sx: Scale, sy: Scale, px: Pixel, py: Pixel) {
        matrix.preScale(sx.value, sy.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun setScale(sx: Scale, sy: Scale, px: Pixel, py: Pixel) {
        matrix.setScale(sx.value, sy.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun postScale(s: Scale) {
        matrix.postScale(s.value, s.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun preScale(s: Scale) {
        matrix.preScale(s.value, s.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun setScale(s: Scale) {
        matrix.setScale(s.value, s.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun postScale(s: Scale, px: Pixel, py: Pixel) {
        matrix.postScale(s.value, s.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun preScale(s: Scale, px: Pixel, py: Pixel) {
        matrix.preScale(s.value, s.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun postScale(s: Scale, p: PixelCoordinate) {
        matrix.postScale(s.value, s.value, p.x.value, p.y.value)
        updateMatrixParams()
    }

    internal fun setScale(s: Scale, p: PixelCoordinate) {
        matrix.setScale(s.value, s.value, p.x.value, p.y.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun preScale(s: Scale, p: PixelCoordinate) {
        matrix.preScale(s.value, s.value, p.x.value, p.y.value)
        isInitialized = true
        updateMatrixParams()
    }

    internal fun setScale(s: Scale, px: Pixel, py: Pixel) {
        matrix.setScale(s.value, s.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun postRotate(d: Degrees) {
        matrix.postRotate(d.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun preRotate(d: Degrees) {
        matrix.preRotate(d.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun setRotate(d: Degrees) {
        matrix.setRotate(d.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun postRotate(d: Degrees, px: Pixel, py: Pixel) {
        matrix.postRotate(d.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun preRotate(d: Degrees, px: Pixel, py: Pixel) {
        matrix.preRotate(d.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun setRotate(d: Degrees, px: Pixel, py: Pixel) {
        matrix.setRotate(d.value, px.value, py.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun postRotate(d: Degrees, p: PixelCoordinate) {
        matrix.postRotate(d.value, p.x.value, p.y.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun preRotate(d: Degrees, p: PixelCoordinate) {
        matrix.preRotate(d.value, p.x.value, p.y.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun setRotate(d: Degrees, p: PixelCoordinate) {
        matrix.setRotate(d.value, p.x.value, p.y.value)
        isInitialized = true
        updateMatrixParams()
    }

    fun invertedMapPixels(x: Pixel, y: Pixel): PixelCoordinate {
        val copy = Matrix()
        matrix.invert(copy)
        val applyMatrixParams = FloatArray(2)

        applyMatrixParams[0] = x.value
        applyMatrixParams[1] = y.value

        copy.mapPoints(applyMatrixParams)

        return PixelCoordinate(
            applyMatrixParams[0].px,
            applyMatrixParams[1].px
        )
    }

    fun mapPixels(x: Pixel, y: Pixel): PixelCoordinate {
        val applyMatrixParams = FloatArray(2)

        applyMatrixParams[0] = x.value
        applyMatrixParams[1] = y.value

        matrix.mapPoints(applyMatrixParams)

        return PixelCoordinate(
            applyMatrixParams[0].px,
            applyMatrixParams[1].px
        )
    }

    fun mapCoordinate(coordinate: PixelCoordinate): PixelCoordinate {
        return mapPixels(coordinate.x, coordinate.y)
    }

    fun mapRect(rect: PixelRect) {
        matrix.mapRect(rect.rectF)
        rect.set(rect.rectF)
    }

    fun applyToCanvas(canvas: Canvas) {
        canvas.setMatrix(matrix)
    }

    private fun updateMatrixParams() {
        matrix.getValues(matrixValues)
        scaleFactor = Scale(
            sqrt(
                matrixValues[Matrix.MSCALE_X].pow(2) +
                        matrixValues[Matrix.MSKEW_Y].pow(2)
            )
        )
        rotationDegrees = Degrees(
            atan2(
                matrixValues[Matrix.MSKEW_X],
                matrixValues[Matrix.MSCALE_X]
            ) * (180f / Math.PI.toFloat())
        )
        translation =
            PixelCoordinate(matrixValues[Matrix.MTRANS_X].px, matrixValues[Matrix.MTRANS_Y].px)
    }

    override fun toString(): String {
        return "PixelMatrix(matrix=$matrix, scaleFactor=$scaleFactor, rotationDegrees=$rotationDegrees, translation=$translation)"
    }
}