package me.oriient.ui.contentview.content

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.util.Log
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.ImageProvider
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.contentColor

private const val TAG = "ArcContent"

/**
 * A [Content] implementation for drawing an arc on a [me.oriient.ui.contentview.ContentView].
 */
open class ArcContent(
    customContentId: ContentId,
    private val center: PixelCoordinate,
    private val radius: Pixel,
    private val direction: Degrees,
    private val opening: Degrees,
    color: ContentColor,
    override val anchorPoint: PixelCoordinate =  center,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId,null, configBlock) {

    constructor(
        center: PixelCoordinate,
        radius: Pixel,
        direction: Degrees,
        opening: Degrees,
        color: ContentColor,
        anchorPoint: PixelCoordinate =  center,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, center, radius, direction, opening, color, anchorPoint, configBlock)

    private val paint = Paint()

    init {
        paint.contentColor = color
        paint.isAntiAlias = true
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {
        canvas.drawArc(
            center.x.value - radius.value,
            center.y.value - radius.value,
            center.x.value + radius.value,
            center.y.value + radius.value,
            direction.value - opening.value / 2,
            opening.value,
            true,
            paint
        )
    }

    override fun toString(): String {
        return "ArcContent(center=$center, radius=$radius, direction=$direction, opening=$opening, anchorPoint=$anchorPoint, paint=$paint)"
    }


}