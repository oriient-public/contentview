package me.oriient.ui.contentview.content

import android.graphics.Bitmap
import android.graphics.Canvas
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.ImageProvider
import me.oriient.ui.contentview.models.*

@Deprecated("Please use isClickedOn() for click area")
open class EmptyContent(
    customContentId: ContentId,
    private val area: PixelRect,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId, null, configBlock) {

    constructor(
        area: PixelRect,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, area, configBlock)

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {
        // do nothing
    }

    override fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        return area.contains(coordinate)
    }
}