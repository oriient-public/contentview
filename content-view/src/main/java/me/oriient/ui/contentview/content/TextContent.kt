package me.oriient.ui.contentview.content

import android.graphics.*
import android.text.TextPaint
import android.text.TextUtils
import android.util.Log
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.contentColor
import me.oriient.ui.contentview.utils.px

private const val TAG = "TextContent"

private val DEFAULT_TYPEFACE = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)

/**
 * A [Content] implementation for drawing text on a [me.oriient.ui.contentview.ContentView].
 */
// TODO: 30/09/2020 support resist scale and rotation when clicking and dragging
open class TextContent(
    customContentId: ContentId,
    private val coordinate: PixelCoordinate,
    private val fontSize: Pixel,
    private val strokeWidth: Pixel = 1.px,
    private val text: List<String>,
    color: ContentColor,
    borderColor: ContentColor? = null,
    typeface: Typeface = DEFAULT_TYPEFACE,
    override val anchorPoint: PixelCoordinate = coordinate,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId, null, configBlock) {

    constructor(
        coordinate: PixelCoordinate,
        fontSize: Pixel,
        strokeWidth: Pixel = 1.px,
        text: List<String>,
        color: ContentColor,
        borderColor: ContentColor? = null,
        typeface: Typeface = DEFAULT_TYPEFACE,
        anchorPoint: PixelCoordinate = coordinate,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, coordinate, fontSize, strokeWidth, text, color, borderColor, typeface, anchorPoint, configBlock)

    private val paint = TextPaint()
    private val borderPaint = TextPaint()
    private val drawBorder = borderColor != null
    private val backgroundRect = PixelRect()

    override val bounds: PixelRect
        get() { return backgroundRect }

    init {
        Log.d(TAG, "TextContent for $text")
        paint.contentColor = color
        paint.textAlign = Paint.Align.CENTER
        paint.typeface = typeface
        paint.isAntiAlias = true
        if (drawBorder) {
            borderPaint.contentColor = borderColor
            borderPaint.style = Paint.Style.STROKE
            borderPaint.textAlign = Paint.Align.CENTER
            borderPaint.typeface = typeface
            borderPaint.isAntiAlias = true
        }
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {
        paint.textSize = fontSize.value
        if (drawBorder) {
            borderPaint.textSize = fontSize.value
            borderPaint.strokeWidth = strokeWidth.value
        }
        // Initial offset put text in the center of container
        var yOffset = fontSize.value / 4
        for (line in text) {
            drawLineOfText(
                canvas,
                line,
                coordinate.x.value,
                coordinate.y.value + yOffset
            )
            yOffset += fontSize.value
        }
    }

    private fun drawLineOfText(
        canvas: Canvas,
        text: String,
        x: Float,
        y: Float
    ) {
        if (LIMIT_TEXT_LENGTH) {
            val labelText = TextUtils.ellipsize(
                text,
                paint,
                70f,
                TextUtils.TruncateAt.END
            ).toString()
            canvas.drawText(labelText, x, y, paint)
            if (drawBorder) {
                canvas.drawText(labelText, x, y, borderPaint)
            }
        } else {
            canvas.drawText(text, x, y, paint)
            if (drawBorder) {
                canvas.drawText(text, x, y, borderPaint)
            }
        }
    }

    private fun getTextBackgroundSize(
        x: Float,
        y: Float,
        text: String,
        paint: TextPaint
    ): Rect {
        val fontMetrics = paint.fontMetrics
        val halfTextLength = paint.measureText(text) / 2 + 5
        return Rect(
            (x - halfTextLength).toInt(),
            (y + fontMetrics.top).toInt(),
            (x + halfTextLength).toInt(),
            (y + fontMetrics.bottom).toInt()
        )
    }

    override fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        updateBackgroundRect()
        return backgroundRect.contains(coordinate)
    }

    private fun updateBackgroundRect() {
        val lineBorders = mutableListOf<Rect>()
        var yOffset = 0f
        for (line in text) {
            lineBorders.add(
                getTextBackgroundSize(
                    this.coordinate.x.value,
                    this.coordinate.y.value + yOffset,
                    line,
                    paint
                )
            )
            yOffset += fontSize.value
        }
        val widestLine = lineBorders.maxByOrNull { it.width() } ?: Rect()

        backgroundRect.set(
            widestLine.left.px,
            lineBorders.first().top.px,
            widestLine.right.px,
            lineBorders.last().bottom.px,
        )
    }

    override fun move(dx: Pixel, dy: Pixel) {
        coordinate.offset(dx, dy)
        updateBackgroundRect()
    }

    override fun toString(): String {
        return "TextContent(coordinate=$coordinate, fontSize=$fontSize, strokeWidth=$strokeWidth, anchorPoint=$anchorPoint, labelText=$text, drawBorder=$drawBorder)"
    }

    companion object {
        private const val LIMIT_TEXT_LENGTH = false
    }
}