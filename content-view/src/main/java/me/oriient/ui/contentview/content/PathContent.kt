package me.oriient.ui.contentview.content

import android.graphics.*
import android.util.Log
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.ImageProvider
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.contentColor
import me.oriient.ui.contentview.utils.px


private const val TAG = "PathContent"

/**
 * A [Content] implementation for drawing a path on a [me.oriient.ui.contentview.ContentView].
 */
open class PathContent(
    customContentId: ContentId,
    private val coordinates: List<PixelCoordinate>,
    color: ContentColor,
    private val width: Pixel,
    private val strokeWidth: Pixel? = null,
    strokeColor: ContentColor? = null,
    private val keepWidth: Boolean = false,
    cornerRadius: Pixel = DEFAULT_CORNER_RADIUS,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
): Content(customContentId, null, configBlock) {

    constructor(
        coordinates: List<PixelCoordinate>,
        color: ContentColor,
        width: Pixel,
        strokeWidth: Pixel? = null,
        strokeColor: ContentColor? = null,
        keepWidth: Boolean = false,
        cornerRadius: Pixel = DEFAULT_CORNER_RADIUS,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ): this(ContentId.random, coordinates, color, width, strokeWidth, strokeColor, keepWidth, cornerRadius, configBlock)

    private val paint = Paint()
    private val strokePaint = Paint()
    private val drawStroke: Boolean

    init {
        paint.contentColor = color
        paint.style = Paint.Style.STROKE
        paint.isDither = true
        paint.strokeJoin = Paint.Join.ROUND
        paint.strokeCap = Paint.Cap.ROUND
        paint.pathEffect = CornerPathEffect(cornerRadius.value)
        paint.isAntiAlias = true
        paint.strokeWidth = width.value

        drawStroke = strokeWidth != null && strokeColor != null

        if (drawStroke) {
            strokePaint.contentColor = strokeColor
            strokePaint.style = Paint.Style.STROKE
            strokePaint.isDither = true
            strokePaint.strokeJoin = Paint.Join.ROUND
            strokePaint.strokeCap = Paint.Cap.ROUND
            strokePaint.pathEffect = CornerPathEffect(cornerRadius.value)
            strokePaint.isAntiAlias = true
            strokePaint.strokeWidth = width.value + 2 * strokeWidth!!.value
        }
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {

        if (coordinates.size < 2) {
            Log.e(TAG, "draw: attempting to draw a route with less than 2 coordinates!")
            return
        }

        var actualWidth = width
        var actualStrokeWidth = strokeWidth

        if (keepWidth) {
            actualWidth = width / transformMatrix.scaleFactor
            if (drawStroke) {
                actualStrokeWidth = strokeWidth!! / transformMatrix.scaleFactor
            }
        }

        paint.strokeWidth = actualWidth.value

        if (drawStroke) {
            strokePaint.strokeWidth = actualWidth.value + 2 * actualStrokeWidth!!.value
        }

        val path = Path()
        val firstCoordinate = coordinates[0]
        path.moveTo(firstCoordinate.x.value, firstCoordinate.y.value)
        for (i in 1 until coordinates.size) {
            val coordinate = coordinates[i]
            path.lineTo(coordinate.x.value, coordinate.y.value)
        }

        if (drawStroke) {
            canvas.drawPath(path, strokePaint)
        }

        canvas.drawPath(path, paint)
    }

    override fun toString(): String {
        return "PathContent(coordinates=$coordinates, width=$width, strokeWidth=$strokeWidth, keepWidth=$keepWidth, drawStroke=$drawStroke)"
    }


    companion object {
        val DEFAULT_CORNER_RADIUS = 5.px
    }
}