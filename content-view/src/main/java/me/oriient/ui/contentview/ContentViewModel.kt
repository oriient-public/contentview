package me.oriient.ui.contentview

import androidx.lifecycle.ViewModel
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.px


private const val TAG = "ContentViewModel"

internal class ContentViewModel: ViewModel() {

    internal val drawingMatrix = PixelMatrix()
    internal val inverseDrawingMatrix = PixelMatrix()
    internal val contentBounds = PixelRect()
    internal val scrollLimitRect = PixelRect()

    internal var clickListener: ((PixelCoordinate, List<ContentId>) -> Unit)? = null
    internal var longClickListener: ((PixelCoordinate, List<ContentId>) -> Unit)? = null
    internal var doubleTapListener: ((PixelCoordinate, List<ContentId>) -> Unit)? = null
    internal var interactionListener: ((TouchEvent) -> Unit)? = null
    internal var scaleListeners: MutableList<MapScaleListener> = mutableListOf()
    internal var dragListener: ((DragEvent) -> Unit)? = null

    internal val contentManager = ContentManager()
    internal val displayMetricsGenerator = DisplayMetricsGenerator()

    internal var rotationEnabled = true
    internal var scrollEnabled = true
    internal var scaleEnabled = true
    internal var decelerationEnabled = true
    internal var contentScrollLimit = 200.px
    internal var minScale = Scale(0f)
    internal var maxScale = Scale(100f)
}