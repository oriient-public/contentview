package me.oriient.ui.contentview.utils

import android.animation.Animator
import android.animation.FloatArrayEvaluator
import android.animation.ValueAnimator
import android.util.Log
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.LinearInterpolator
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.models.Scale
import me.oriient.ui.contentview.models.ViewFocusState
import kotlin.math.abs


private const val TAG = "MatrixAnimator"

internal class MatrixAnimator {

    private var numberOfValuesToAnimate: Int = 6
    private val currentValues = FloatArray(numberOfValuesToAnimate)
    private val currentSourceValues = FloatArray(numberOfValuesToAnimate)
    private val currentTargetValues = FloatArray(numberOfValuesToAnimate)

    private val floatArrayEvaluator = FloatArrayEvaluator(currentValues)
    private val accelerateDecelerateInterpolator = AccelerateDecelerateInterpolator()
    private val linearInterpolator = LinearInterpolator()
    private val animator = ValueAnimator.ofObject(floatArrayEvaluator, currentSourceValues, currentTargetValues)

    private var targetViewFocusState: ViewFocusState? = null
    private val matrix = PixelMatrix()

    private val listeners = mutableListOf<(PixelMatrix) -> Unit>()

    init {

        animator.addUpdateListener {
            targetViewFocusState?.let {
                val viewWidth: Pixel = it.width
                val viewHeight: Pixel = it.height
                val centerX = Pixel(currentValues[2])
                val centerY = Pixel(currentValues[3])
                val offsetX = Pixel(currentValues[4])
                val offsetY = Pixel(currentValues[5])

                matrix.apply {
                    reset()
                    postScale(Scale(currentValues[1]), Scale(currentValues[1]), centerX, centerY)
                    postRotate(Degrees(currentValues[0]%360f - 180f), centerX, centerY)
                    postTranslate((viewWidth.value / 2f).px - centerX + offsetX, (viewHeight.value / 2f).px - centerY + offsetY)
                }

                listeners.copyOf().forEach { it.invoke(matrix) }
            }
        }

        // Use in case we want to monitor the animation state
        animator.addListener(object: Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {  }

            override fun onAnimationEnd(animation: Animator?) {
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
            }
        })
    }

    fun animate(
        source: ViewFocusState,
        destination: ViewFocusState,
        durationMillis: Long,
        animationMode: AnimationMode
    ) {
        val sourceRotation = source.rotation.value + 180f
        val originalDestinationRotation = destination.rotation.value + 180f
        var destinationRotation = originalDestinationRotation
        var delta = abs(sourceRotation - originalDestinationRotation)

        if (delta > abs(sourceRotation - (originalDestinationRotation - 360f))) {
            delta = abs(sourceRotation - (originalDestinationRotation - 360f))
            destinationRotation = (originalDestinationRotation - 360f)
        }

        if (delta > abs(sourceRotation - (originalDestinationRotation + 360f))) {
            destinationRotation = (originalDestinationRotation + 360f)
        }

        val destinationValues = FloatArray(numberOfValuesToAnimate)
        destinationValues[0] = destinationRotation
        destinationValues[1] = destination.scale.value
        destinationValues[2] = destination.center.x.value
        destinationValues[3] = destination.center.y.value
        destinationValues[4] = destination.offset.x.value
        destinationValues[5] = destination.offset.y.value

        val sourceValues = FloatArray(numberOfValuesToAnimate)
        sourceValues[0] = sourceRotation
        sourceValues[1] = source.scale.value
        sourceValues[2] = source.center.x.value
        sourceValues[3] = source.center.y.value
        sourceValues[4] = source.offset.x.value
        sourceValues[5] = source.offset.y.value

        targetViewFocusState = destination

        sourceValues.copyInto(currentSourceValues)
        destinationValues.copyInto(currentTargetValues)

        animator.duration = durationMillis
        animator.interpolator = when (animationMode) {
            AnimationMode.Linear -> linearInterpolator
            AnimationMode.AccelerateDecelerate -> accelerateDecelerateInterpolator
            else -> linearInterpolator
        }

        animator.start()
    }

    fun getCurrentOffset(): PixelCoordinate {
        return PixelCoordinate(Pixel(currentValues[4]), Pixel(currentValues[5]))
    }

    fun addListener(listener: (PixelMatrix) -> Unit) {
        listeners.add(listener)
    }

    fun removeListener(listener: (PixelMatrix) -> Unit) {
        listeners.remove(listener)
    }

    fun cancelAnimation() {
        animator.cancel()
    }
}
