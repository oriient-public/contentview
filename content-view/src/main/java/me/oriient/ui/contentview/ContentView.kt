package me.oriient.ui.contentview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.Log
import android.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import me.oriient.ui.contentview.content.ImageContent
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.models.DragEvent
import me.oriient.ui.contentview.utils.*
import kotlin.math.*

// TODO: check usage from Java

/**
 * This view is a generic way to display visual content while supporting translation (scrolling), rotation and zoom.
 * See module documentation for more details.
 */

@Suppress("unused", "MemberVisibilityCanBePrivate")
class ContentView(
    context: Context,
    attrs: AttributeSet? = null,
): View(context, attrs),
   ViewModelStoreOwner,
   RotationGestureDetector.OnRotationGestureListener {

    private var isScrollEvent = false
    private var didLongClick = false
    private var didClick = false

    private val animateMatrix = PixelMatrix()

    private var gestureDetector: GestureDetector
    private var scaleDetector: ScaleGestureDetector
    private val rotationDetector = RotationGestureDetector(this)

    private val matrixAnimator = MatrixAnimator()
    private val locationOnScreen = IntArray(2)

    private var draggedContent: DraggedContent? = null
    private val ignoreMatrix = PixelMatrix()

    private val decelerationHandler: Handler
    private var velocityTracker: VelocityTracker? = null
    private var decelerationAnimationCanceled = false

    private val imageRequests = mutableMapOf<ContentId, ImageRequest.Deferred>()

    private val coroutineScope = CoroutineScope(Dispatchers.Unconfined)

    /**
     * The color of the view below the background image.
     */
    var backgroundColor = ContentColor.Int(Color.WHITE)

    /**
     * If set to `true`, drag events which end out of the borders of the background will cause the
     * dragged [Content] to return to its original position and will send a [DragEvent.DragCanceled] event.
     * Otherwise, the dragged [Content] will remain at the edge of the border.
     * 'false' by default.
     */
    var cancelOnDragOutOfContentBounds = false

    var enableLongClickWhileDragging = true
    private val longClickWhileDragHandler: Handler

    private val model: ContentViewModel = ViewModelProvider(this)[ContentViewModel::class.java]

    /**
     * In order to provide better performance, the [ContentView] class allows you to manage images externally.
     * Every time a [Content] is drawn, the [ContentView] will request an image for it.
     * If an image is indeed required, the [ImageProvider] should either provide it synchronously by returning [ImageRequest.Bitmap]
     * or asynchronously by returning [ImageRequest.Deferred]. The relevant [Content] will be redrawn once the [ImageRequest.Deferred] will return the image.
     * If no image is required, the image provider should return `null`.
     */
    var imageProvider: ImageProvider? = null

    /**
     * Describes the rotation of the [ContentView] background in degrees.
     */
    val rotation: Degrees
        get() { return -model.displayMetricsGenerator.rotationDegrees }

    val translationX: Pixel
        get() = model.displayMetricsGenerator.translationX

    val translationY: Pixel
        get() = model.displayMetricsGenerator.translationY

    /**
     * Enables and disables the rotation of the [ContentView] background.
     * `true` by default.
     */
    var rotationEnabled: Boolean
        get() { return model.rotationEnabled }
        set(value) { model.rotationEnabled = value }
    /**
     * Enables and disables the scrolling of the [ContentView] background.
     * `true` by default.
     */
    var scrollEnabled: Boolean
        get() { return model.scrollEnabled }
        set(value) { model.scrollEnabled = value }

    /**
     * Enables and disables the scaling of the [ContentView] background.
     * `true` by default.
     */
    var scaleEnabled: Boolean
        get() { return model.scaleEnabled }
        set(value) { model.scaleEnabled = value }

    /**
     * Enables and disables the deceleration of the [ContentView] background after scrolling.
     * `true` by default.
     */
    var decelerationEnabled: Boolean
        get() { return model.decelerationEnabled }
        set(value) { model.decelerationEnabled = value }

    /**
     * Indicates how far beyond its border can the background be scrolled.
     */
    var contentScrollLimit: Pixel
        get() { return model.contentScrollLimit }
        set(value) { model.contentScrollLimit = value }

    /**
     * Indicates the maximum number of pixels that can fit into the width of the view.
     *
     * Higher value means that the map can zoom out further.
     */
    // this value should only be used for API representation.
    // The canvas actually works with ViewModel.minScale (in Scale units).
    var maxPixelsOnScreen: PixelsInViewWidth
        get() { return model.minScale.toPixelsOnViewWidth(width.px) }
        set(value) { model.minScale = value.toScale(width.px) }

    /**
     * Indicates the minimal number of pixels that can fit into the width of the view.
     *
     * Lower value means that the map can zoom in further.
     */
    // this value should only be used for API representation.
    // The canvas actually works with ViewModel.maxScale (in Scale units).
    var minPixelsOnScreen: PixelsInViewWidth
        get() { return model.maxScale.toPixelsOnViewWidth(width.px) }
        set(value) { model.maxScale = value.toScale(width.px) }

    /**
     * Indicates the current number of pixels that are being displayed in the width of the view.
     */
    // this value should only be used for API representation.
    // The canvas actually works with ViewModel.displayMetricsGenerator.scaleFactor (in Scale units).
    val scale: PixelsInViewWidth
        get() { return model.displayMetricsGenerator.scaleFactor.toPixelsOnViewWidth(width.px) }

    /**
     * Returns matrix will all current transformations including scale, rotation
     */
    internal val currentTransformations: PixelMatrix
        get() {
            return model.displayMetricsGenerator.matrix
        }

    var statLogConfig = StatLogConfig()
    private val statData = StatData()

    init {
        scaleDetector = ScaleGestureDetector(context, ScaleListener())
        val moveListener = MoveListener()
        gestureDetector = GestureDetector(context, moveListener)
        matrixAnimator.addListener { matrix ->
            applyNewMatrix(matrix)
            model.scaleListeners.forEach { it.onScaleChanged(matrix) }
        }
        longClickWhileDragHandler = Handler(Looper.getMainLooper()) {
            moveListener.onLongPress(it.obj as MotionEvent)
            true
        }

        decelerationHandler = Handler(Looper.getMainLooper()) {
            if (decelerationAnimationCanceled) {
                return@Handler true
            }
            val message = it.obj as DecelerationData
            val totalUpdates = (DECELERATE_ANIMATION_DURATION_MILLI / DECELERATE_TIME_BETWEEN_UPDATES_MILLI).toInt()
            if (message.count <= totalUpdates) {
                val dx = message.velocityX / (1000 / DECELERATE_TIME_BETWEEN_UPDATES_MILLI)
                val dy = message.velocityY / (1000 / DECELERATE_TIME_BETWEEN_UPDATES_MILLI)
                model.drawingMatrix.postTranslate(dx.px, dy.px)
                keepPositionInScrollLimit()
                invalidate()
                sendDecelerationMessage(message.count, decreaseVelocity(message.velocityX, message.count), decreaseVelocity(message.velocityY, message.count))
            }
            true
        }
    }

    private fun applyNewMatrix(it: PixelMatrix) {
        model.drawingMatrix.set(it)
        post { invalidate() }
    }

    private fun decreaseVelocity(velocity: Float, count: Int): Float {
        val t = count.toDouble() / (DECELERATE_ANIMATION_DURATION_MILLI / DECELERATE_TIME_BETWEEN_UPDATES_MILLI).toDouble()
        return velocity - (velocity * t.pow(2.0 * DECELERATION_FACTOR).toFloat())
    }

    private fun sendDecelerationMessage(count: Int, velocityX: Float, velocityY: Float) {
        decelerationAnimationCanceled = false
        decelerationHandler.sendMessageDelayed(
            decelerationHandler.obtainMessage(0, DecelerationData(count + 1, velocityX, velocityY)),
            DECELERATE_TIME_BETWEEN_UPDATES_MILLI
        )
    }

    /**
     * Set the borders of the background manually. Otherwise set by the background image in [setBackground].
     */
    fun setContentBounds(bounds: PixelRect) {
        model.contentBounds.set(bounds)
        contentScrollLimit = if (width > height) {
            min(bounds.height.value / 2, contentScrollLimit.value).px
        } else {
            min(bounds.width.value / 2, contentScrollLimit.value).px
        }
    }

    /**
     * The borders of the background.
     */
    fun getContentBounds(): PixelRect {
        return model.contentBounds
    }

    /**
     * Calling this method will move the center of the view to be at the center of provided coordinates list
     * with optimal scale to fit all of them.
     * The transition can be animated. Multiple animations will be chained. Animations are cancelled upon touch.
     *
     * @param coordinates a list of PixelCoordinates to center the view on
     * @param marginPixels margin in pixels of the [ContentView] that will be considered in optimal scale calculation,
     * so after scale all items would fit in the area + defined margin.
     * @param animationDuration the duration of the animation in milliseconds (default is 1500 milliseconds)
     * @param animationMode the mode of the animation (default is linear animation)
     *
     * @since 1.4.0
     */
    fun centerOnPixelsList(
        coordinates: List<PixelCoordinate>,
        marginPixels: Pixel = 0.px,
        animationDuration: Long = 1500L,
        animationMode: AnimationMode = AnimationMode.Linear
    ) {
        if (coordinates.size <= 1) {
            return
        }

        val converted = coordinates.map { model.drawingMatrix.mapCoordinate(it) }

        val maxX = converted.maxOf { it.x }
        val minX = converted.minOf { it.x }
        val maxY = converted.maxOf { it.y }
        val minY = converted.minOf { it.y }
        val width = abs(maxX.value - minX.value)
        val height = abs(maxY.value - minY.value)
        val targetWidth = this.width - (2 * marginPixels.value)
        val targetHeight = this.height - (2 * marginPixels.value)

        val center = PixelCoordinate(
            x = (minX.value + width / 2).px,
            y = (minY.value + height / 2).px
        )

        val revertedCenter = model.drawingMatrix.invertedMapPixels(center.x, center.y)

        var targetScale = Float.MAX_VALUE
        if (width < 1.0.ulp) {
            if (height < 1.0.ulp) {
                // items are in the same place
            } else {
                targetScale = currentTransformations.scaleFactor.value * targetHeight / height
            }
        } else {
            targetScale = if (height < 1.0.ulp) {
                currentTransformations.scaleFactor.value * targetWidth / width
            } else {
                min(
                    currentTransformations.scaleFactor.value * targetWidth / width,
                    currentTransformations.scaleFactor.value * targetHeight / height
                )
            }
        }

        val targetZoomMeters = PixelsInViewWidth(max(
            minPixelsOnScreen.value,
            Scale(targetScale).toPixelsOnViewWidth(this.width.px).value
        ))

        val currentRotationDegrees = -model.drawingMatrix.rotationDegrees
        centerOnPixels(
            x = revertedCenter.x,
            y = revertedCenter.y,
            rotation = currentRotationDegrees,
            scale = targetZoomMeters,
            animationDuration = animationDuration,
            animationMode = animationMode
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if (ev == null) {
            return false
        }


        matrixAnimator.cancelAnimation()
        decelerationAnimationCanceled = true

        velocityTracker?.addMovement(ev)

        model.displayMetricsGenerator.isBeingTouched = ev.action != MotionEvent.ACTION_UP

        val screenCoordinate = ev.toScreenCoordinate()

        ev.toTouchEvent()?.let { event ->
            model.interactionListener?.invoke(event)
        }

        if (ev.pointerCount >= 2) {
            val x1 = ev.getX(0)
            val y1 = ev.getY(0)
            val x2 = ev.getX(1)
            val y2 = ev.getY(1)

            val x = (x1 + x2) / 2f
            val y = (y1 + y2) / 2f

            model.displayMetricsGenerator.focusX = x.px
            model.displayMetricsGenerator.focusY = y.px

            draggedContent?.let {
                model.dragListener?.invoke(DragEvent.DragEnded(it))
                draggedContent = null
            }
        }

        scaleDetector.onTouchEvent(ev)
        gestureDetector.onTouchEvent(ev)
        rotationDetector.onTouchEvent(ev)

        if (ev.action == MotionEvent.ACTION_UP) {
            longClickWhileDragHandler.removeMessages(0)
            draggedContent?.let { draggedContent ->
                val contentCoordinate = getContentCoordinate(screenCoordinate)
                if (model.contentBounds.contains(contentCoordinate) || !cancelOnDragOutOfContentBounds) {
                    model.dragListener?.invoke(DragEvent.DragEnded(draggedContent))
                } else {
                    model.dragListener?.invoke(DragEvent.DragCanceled(draggedContent))
                    draggedContent.content.move(
                        draggedContent.originalPosition.x - draggedContent.position.x,
                        draggedContent.originalPosition.y - draggedContent.position.y
                    )
                    model.contentManager.removeContent(listOf(draggedContent.content.contentId)) {
                        model.contentManager.addContent(listOf(draggedContent.content)) { }
                        invalidate()
                    }
                }
            } ?: let {
                if (isScrollEvent) {
                    velocityTracker?.run {
                        val scrollPointerId = ev.getPointerId(0)
                        computeCurrentVelocity(1000, 8000f)
                        val velocityY = getYVelocity(scrollPointerId)
                        val velocityX = getXVelocity(scrollPointerId)
                        sendDecelerationMessage(0, velocityX, velocityY)
                    }
                    velocityTracker?.recycle()
                    velocityTracker = null
                }
            }

            isScrollEvent = false
            didLongClick = false
            draggedContent = null
        }

        return true
    }

    private fun onClick(event: MotionEvent): Boolean {
        if (didLongClick) {
            return false
        }

        draggedContent?.let {
            Log.d(TAG, "onClick: dragging in progress")
            return false
        }

        return invokeListenerOnContentInsideCoordinates(
            event =  event,
            listener = model.clickListener,
            listenerName = "onClick"
        )
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        Log.d(TAG, "onSizeChanged() called with: w = [$w], h = [$h], oldw = [$oldw], oldh = [$oldh]")
        super.onSizeChanged(w, h, oldw, oldh)

        model.displayMetricsGenerator.run {
            focusX = (width / 2f).px
            focusY = (height / 2f).px
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.run {

            canvas.drawColor(backgroundColor.toAndroidColorInt())

            model.displayMetricsGenerator.updateMatrix(model.drawingMatrix)
            model.drawingMatrix.applyToCanvas(canvas)

            save()
            drawContent(this)
            restore()


        }
    }

    private fun drawContent(canvas: Canvas) {
        synchronized(model.contentManager) {
            val rotationDegrees = model.displayMetricsGenerator.rotationDegrees
            if (statLogConfig.enabled) {
                statData.reset()
                if (statLogConfig.count) {
                    statData.totalContent = model.contentManager.getContent().size
                }
            }
            for (content in model.contentManager.getContent()) {
                if (shouldDrawContent(content)) {
                    canvas.save()
                    if (statLogConfig.count) {
                        statData.shouldDraw++
                    }
                    imageProvider?.run {
                        content.imageIdentifier?.let { imageIdentifier ->
                            provideImage(context.applicationContext, imageIdentifier)?.let { request ->
                                when (request) {
                                    is ImageRequest.Bitmap -> {
                                        request.anchorPoint?.let {
                                            content._anchorPoint = it
                                        }
                                        if (statLogConfig.count) {
                                            statData.withImage++
                                        }
                                        doDrawContent(canvas, content, rotationDegrees, request.value)
                                    }
                                    is ImageRequest.Deferred -> {
                                        if (!imageRequests.containsKey(content.contentId)) {
                                            imageRequests[content.contentId] = request
                                            coroutineScope.launch {
                                                if (statLogConfig.count) {
                                                    statData.deferred++
                                                }
                                                if (request.loader.invoke()) {
                                                    model.contentManager.getContent(content.contentId)?.let {
                                                        updateContent(listOf(it))
                                                    }
                                                } else {
                                                    if (statLogConfig.count) {
                                                        statData.notLoaded++
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } ?: let {
                                if (statLogConfig.count) {
                                    statData.imageNotProvided++
                                }
                                doDrawContent(canvas, content, rotationDegrees, null)
                            }
                        } ?: let {
                            if (statLogConfig.count) {
                                statData.withoutImage++
                            }
                            doDrawContent(canvas, content, rotationDegrees, null)
                        }
                    } ?: let {
                        if (statLogConfig.count) {
                            statData.withoutImage++
                        }
                        doDrawContent(canvas, content, rotationDegrees, null)
                    }

                    canvas.restore()
                }
            }
        }

        if (statLogConfig.enabled) {
            Log.d(statLogConfig.tag, statData.toString())
        }
    }

    private fun doDrawContent(canvas: Canvas, content: Content, rotation: Degrees, image: Bitmap?) {
        val scaleFactor = clipScaleFactor(model.displayMetricsGenerator.scaleFactor, content)

        if (content.options.resistRotation) {
            canvas.rotate(
                rotation.value,
                content.anchorPoint.x.value,
                content.anchorPoint.y.value,
            )
        }

        if (content.options.resistScale) {
            canvas.scale(
                1 / scaleFactor.value,
                1 / scaleFactor.value,
                content.anchorPoint.x.value,
                content.anchorPoint.y.value,
            )
        }
        content.draw(canvas, model.displayMetricsGenerator.matrix, image)
    }

    private fun clipScaleFactor(
        scaleFactor: Scale,
        content: Content
    ): Scale {
        // note that PixelsInViewWidth behaves in the opposite way to the scale -
        // the bigger the scale the less meters you see and vice versa.
        return when {
            scaleFactor < content.options.maxResistScale.toScale(width.px) -> {
                content.options.maxResistScale.toScale(width.px)
            }
            scaleFactor > content.options.minResistScale.toScale(width.px) -> {
                content.options.minResistScale.toScale(width.px)
            }
            else -> {
                scaleFactor
            }
        }
    }

    // note that PixelsInViewWidth behaves in the opposite way to the scale -
    // the bigger the scale the less meters you see and vice versa.
    private fun shouldDrawContent(
        content: Content
    ) = model.displayMetricsGenerator.scaleFactor < content.options.minZoom.toScale(width.px) && model.displayMetricsGenerator.scaleFactor > content.options.maxZoom.toScale(width.px)

    /**
     * Adds and updates content on the [ContentView].
     * This method will not remove any existing content.
     * New content will be added and existing content with the same [ContentId] will be replaced.
     */
    fun setOrUpdateContent(userContent: List<Content>) {
        model.contentManager.setOrUpdateContent(userContent) {
            post { invalidate() }
        }
    }

    fun updateContent(userContent: List<Content>) {
        model.contentManager.updateContent(userContent) {
            post { invalidate() }
        }
    }

    fun setBackground(content: ImageContent) {
        content.bounds?.let {
            setContentBounds(it)
        } ?: throw Exception("Background ${ImageContent::class.simpleName} must have bounds!")
        model.contentManager.setOrUpdateContent(listOf(content)) {
            post { invalidate() }
        }
    }

    /**
     * Set content to be displayed on the [ContentView].
     * This method will remove all the existing content and will replace it.
     */
    fun setContent(userContent: List<Content>) {
        model.contentManager.setContent(userContent) {
            post { invalidate() }
        }
    }

    /**
     * Add content to be displayed on the [ContentView].
     * This method will not remove or update any existing content.
     * Note that content with the same [ContentId] will be duplicated and not replaced.
     * For updating existing content please use [setOrUpdateContent].
     */
    fun addContent(userContent: List<Content>) {
        model.contentManager.addContent(userContent) {
            post { invalidate() }
        }
    }

    /**
     * Removes content displayed on the [ContentView].
     */
    fun removeContent(userContent: List<ContentId>) {
        model.contentManager.removeContent(userContent) {
            post { invalidate() }
        }
    }

    fun removeAllContent() {
        model.contentManager.removeAll {
            post { invalidate() }
        }
    }

    /**
     * Sets a listener to respond to click events.
     * Note: [Content] must be marked with [DrawingOptions.clickable] `true` to receive click events.
     */
    fun setClickListener(listener: ContentViewClickListener) {
        model.clickListener = { pixels, inContent -> listener.onClick(pixels, inContent) }
    }

    /**
     * Sets a listener to respond to click events.
     * Note: [Content] must be marked with [DrawingOptions.clickable] `true` to receive click events.
     */
    fun setClickListener(listener: (PixelCoordinate, List<ContentId>) -> Unit) {
        model.clickListener = listener
    }

    /**
     * Removes the click listener.
     */
    fun removeClickListener() {
        model.clickListener = null
    }

    /**
     * Sets a listener to respond to long click events.
     * Note: [Content] must be marked with [DrawingOptions.clickable] `true` to receive long click events.
     */
    fun setLongClickListener(listener: ContentViewLongClickListener) {
        model.longClickListener = { pixels, inContent -> listener.onLongClick(pixels, inContent) }
    }

    /**
     * Sets a listener to respond to long click events.
     * Note: [Content] must be marked with [DrawingOptions.clickable] `true` to receive long click events.
     */
    fun setLongClickListener(listener: (PixelCoordinate, List<ContentId>) -> Unit) {
        model.longClickListener = listener
    }

    /**
     * Sets a listener to respond to double tap events.
     * Note: [Content] must be marked with [DrawingOptions.clickable] `true` to receive double tap events.
     */
    fun setDoubleTapListener(listener: (PixelCoordinate, List<ContentId>) -> Unit) {
        model.doubleTapListener = listener
    }

    /**
     * Removes long click listeners.
     */
    fun removeLongClickListener() {
        model.longClickListener = null
    }

    /**
     * Adds an interaction listener. Every touch of the [ContentView] is considered an interaction.
     */
    fun setInteractionListener(listener: ContentViewInteractionListener) {
        model.interactionListener = { event -> listener.onTouchEvent(event) }
    }

    /**
     * Adds an interaction listener. Every touch of the [ContentView] is considered an interaction.
     */
    fun setInteractionListener(listener: (TouchEvent) -> Unit) {
        model.interactionListener = listener
    }

    /**
     * Removes the interaction listener.
     */
    fun removeInteractionListener() {
        model.interactionListener = null
    }

    /**
     * Sets a listener to respond to drag events.
     * Note: [Content] must be marked with [DrawingOptions.draggable] `true` to receive drag events.
     */
    fun setDragListener(listener: ContentViewDragListener) {
        model.dragListener = { dragEvent -> listener.onDragEvent(dragEvent) }
    }

    /**
     * Sets a listener to respond to drag events.
     * Note: [Content] must be marked with [DrawingOptions.draggable] `true` to receive drag events.
     */
    fun setDragListener(listener: (DragEvent) -> Unit) {
        model.dragListener = listener
    }

    /**
     * Removes the drag listener.
     */
    fun removeDragListener() {
        model.dragListener = null
    }

    /**
     *
     * Determines if the maximum zoom has been reached based on the current scale difference.
     * @param minimalScaleDiff the minimum scale difference that is enough to say that maxScale has been reached.
     * Default value set to 0.01
     * @return true if the maximum zoom level has been reached, false otherwise.
     */
    fun isMaxZoomReached(minimalScaleDiff: Double = MINIMAL_SCALE_DIFF_THRESHOLD): Boolean {
        return abs(scale.value - minPixelsOnScreen.value) < minimalScaleDiff
    }

    /**
     * Calling this method will move the view's top left corner to be at the provided coordinate
     * at the provided scale and rotation.
     * The transition is animated (optional). Multiple animations will be chained.
     * Animations are cancelled upon touch.
     */
    @Deprecated("Use the new centerOnPixels() method with animationMode instead")
    fun translateToPixels(
        x: Pixel = this.model.drawingMatrix.invertedMapPixels(0.px, 0.px).x,
        y: Pixel = this.model.drawingMatrix.invertedMapPixels(0.px, 0.px).y,
        scale: PixelsInViewWidth = this.scale,
        rotation: Degrees = this.rotation,
        animate: Boolean = true
    ) {
        val zeroTranslation = this.model.drawingMatrix.invertedMapPixels(0.px, 0.px)
        val midTranslation = this.model.drawingMatrix.invertedMapPixels(width.px/2, height.px/2)

       centerOnPixels(
           x + (midTranslation.x - zeroTranslation.x),
           y + (midTranslation.y - zeroTranslation.y),
           scale,
           rotation,
           animate
       )
    }

    /**
     * Calling this method will move the center of the view to be at the provided coordinate at
     * the provided scale and rotation.
     * The transition is animated. Multiple animations will be chained. Animations are cancelled upon touch.
     */
    @Deprecated("Use the new centerOnPixels() method with animationMode instead")
    fun centerOnPixels(
        x: Pixel = width.px / 2,
        y: Pixel = height.px / 2,
        scale: PixelsInViewWidth = 1.x.toPixelsOnViewWidth(width.px),
        rotation: Degrees = 0.dg,
        animate: Boolean
    ) {
        centerOnPixels(x = x, y = y, scale = scale, rotation = rotation, animationDuration = 1500L, animationMode =  if (animate) AnimationMode.AccelerateDecelerate else AnimationMode.None)
    }

    /**
     * Calling this method will move the center of the view to be at the provided coordinate at
     * the provided scale and rotation.
     * The transition can be animated. Multiple animations will be chained. Animations are cancelled upon touch.
     */
    fun centerOnPixels(
        x: Pixel = width.px / 2,
        y: Pixel = height.px / 2,
        scale: PixelsInViewWidth = 1.x.toPixelsOnViewWidth(width.px),
        rotation: Degrees = 0.dg,
        offsetX: Pixel = 0.px,
        offsetY: Pixel = 0.px,
        animationDuration: Long = 1500L,
        animationMode: AnimationMode = AnimationMode.Linear
    ) {
        val normalizedRotation = normalizeAngle(rotation.value).dg
        when (animationMode != AnimationMode.None && model.drawingMatrix.isInitialized) {
            true -> {
                val destination = ViewFocusState(
                    scale.toScale(width.px),
                    normalizedRotation,
                    PixelCoordinate(x, y),
                    width.px,
                    height.px,
                    PixelCoordinate(offsetX, offsetY)
                )

                val currentOffset = matrixAnimator.getCurrentOffset()

                val currentViewState = ViewFocusState(
                    model.drawingMatrix.scaleFactor,
                    -model.drawingMatrix.rotationDegrees,
                    model.drawingMatrix.invertedMapPixels(width.px/2 + currentOffset.x, height.px/2 + currentOffset.y),
                    width.px,
                    height.px,
                    currentOffset
                )

                matrixAnimator.animate(
                    currentViewState,
                    destination,
                    animationDuration,
                    animationMode
                )
            }
            false -> {
                matrixAnimator.cancelAnimation()

                val destinationMatrix = animateMatrix.apply {
                    reset()
                    postScale(scale.toScale(width.px), scale.toScale(width.px), x, y)
                    postRotate(normalizedRotation, x, y)
                    postTranslate( (width / 2f).px - x + offsetX,(height / 2f).px - y + offsetY)
                }

                applyNewMatrix(destinationMatrix)
            }
        }
    }

    /**
     * Adds a listener to react on scale events.
     *
     * @since 1.4.0
     */
    fun addScaleListener(listener: MapScaleListener) {
        if (model.scaleListeners.contains(listener)) {
            return
        }

        model.scaleListeners.add(listener)
    }

    /**
     * Removes a scale listener
     *
     * @since 1.4.0
     */
    fun removeScaleListener(listener: MapScaleListener) {
        model.scaleListeners.remove(listener)
    }

    private fun getContentCoordinate(screenCoordinate: PixelCoordinate): PixelCoordinate {
        getLocationOnScreen(locationOnScreen)
        val contentCoordinate =
            screenCoordinate.copy()
        contentCoordinate.offset(
            -locationOnScreen[0].px,
            -locationOnScreen[1].px
        )
        return model.displayMetricsGenerator.inverseMatrix.mapCoordinate(contentCoordinate)
    }

    private fun getContentCoordinateIgnoring(
        screenCoordinate: PixelCoordinate,
        anchor: PixelCoordinate,
        ignoreScale: Boolean = false,
        ignoreRotation: Boolean = false
    ): PixelCoordinate {
        getLocationOnScreen(locationOnScreen)
        val contentCoordinate =
            screenCoordinate.copy()
        contentCoordinate.offset(
            -locationOnScreen[0].px,
            -locationOnScreen[1].px
        )
        ignoreMatrix.set(model.displayMetricsGenerator.inverseMatrix)
        if (ignoreScale) {
            ignoreMatrix.postScale(model.drawingMatrix.scaleFactor, anchor.x, anchor.y)
        }
        if (ignoreRotation) {
            ignoreMatrix.postRotate(-model.drawingMatrix.rotationDegrees, anchor.x, anchor.y)
        }
        return ignoreMatrix.mapCoordinate(contentCoordinate)
    }

    private fun revertScale(screenCoordinate: PixelCoordinate): PixelCoordinate {
        getLocationOnScreen(locationOnScreen)
        val contentCoordinate =
            screenCoordinate.copy()
        contentCoordinate.offset(
            locationOnScreen[0].px,
            locationOnScreen[1].px
        )
        return model.drawingMatrix.mapCoordinate(contentCoordinate)
    }

    override fun onRotation(angle: Float): Boolean {
        if (rotationEnabled) {

            if (draggedContent != null) {
                Log.d(TAG, "onRotation: ignoring while dragging")
                return false
            }

            model.drawingMatrix.postRotate(
                -angle.dg,
                model.displayMetricsGenerator.focusX,
                model.displayMetricsGenerator.focusY
            )

            keepPositionInScrollLimit()

            invalidate()
            return true
        }
        return false
    }

    inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {

            if (scaleEnabled) {

                if (draggedContent != null) {
                    Log.d(TAG, "onScale: ignoring while dragging")
                    return false
                }

                val factor = model.displayMetricsGenerator.scaleFactor * detector.scaleFactor.x

                if (factor.value in model.minScale.value..model.maxScale.value) {

                    model.drawingMatrix.postScale(
                        detector.scaleFactor.x,
                        detector.scaleFactor.x,
                        model.displayMetricsGenerator.focusX,
                        model.displayMetricsGenerator.focusY
                    )

                    keepPositionInScrollLimit()
                }

                invalidate()
                model.scaleListeners.forEach { it.onScaleChanged(currentTransformations) }
                return true
            }
            return false
        }
    }

    inner class MoveListener : GestureDetector.SimpleOnGestureListener() {

        override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
            draggedContent?.let { contentWrapper ->
                return contentWrapper.content.bounds?.copy()?.let { bounds ->
                    longClickWhileDragHandler.removeMessages(0)

                    model.contentManager.removeContent(listOf(contentWrapper.content.contentId)) {}
                    model.drawingMatrix.invert(model.inverseDrawingMatrix)
                    val destination =
                        model.inverseDrawingMatrix.mapPixels(e2.rawX.px, e2.rawY.px)
                    val dX = destination.x - contentWrapper.position.x
                    val dY = destination.y - contentWrapper.position.y
                    contentWrapper.content.move(dX, dY)

                    val matrix = PixelMatrix()

                    if (contentWrapper.content.options.resistScale) {
                        matrix.postScale(
                            1 / model.drawingMatrix.scaleFactor,
                            contentWrapper.content.anchorPoint
                        )
                    }

                    if (contentWrapper.content.options.resistRotation) {
                        matrix.postRotate(
                            -model.drawingMatrix.rotationDegrees,
                            contentWrapper.content.anchorPoint
                        )
                    }

                    matrix.mapRect(bounds)
                    val distances = model.contentBounds.diff(bounds)

                    val distanceFromTop = distances.top
                    val distanceFromBottom = distances.bottom
                    val distanceFromLeft = distances.left
                    val distanceFromRight = distances.right

                    var xCorrection = 0.px
                    var yCorrection = 0.px

                    if (distanceFromTop < 0.px) {
                        yCorrection = distanceFromTop
                    } else if (distanceFromBottom < 0.px) {
                        yCorrection = -distanceFromBottom
                    }

                    if (distanceFromLeft < 0.px) {
                        xCorrection = distanceFromLeft
                    } else if (distanceFromRight < 0.px) {
                        xCorrection = -distanceFromRight
                    }

                    contentWrapper.content.move(-xCorrection, -yCorrection)

                    contentWrapper.position.offset(dX - xCorrection, dY - yCorrection)

                    model.contentManager.addContent(listOf(contentWrapper.content)) { }

                    if (enableLongClickWhileDragging) {
                        longClickWhileDragHandler.removeMessages(0)
                        longClickWhileDragHandler.sendMessageDelayed(
                            longClickWhileDragHandler.obtainMessage(0, e2),
                            ViewConfiguration.getLongPressTimeout().toLong()
                        )
                    }

                    invalidate()

                    true
                } ?: let {
                    Log.e(TAG, "dragged content has no bounds!")
                    false
                }


            } ?: let {
                if (e1.pointerCount == 1) {
                    val screenCoordinate = e1.toScreenCoordinate()
                    getContentCoordinate(screenCoordinate).let { contentCoordinate ->
                        if (model.contentBounds.contains(contentCoordinate)) {
                            getContentAtCoordinate(
                                contentCoordinate,
                                screenCoordinate
                            ).forEach { content ->
                                if (content.options.draggable) {
                                    model.drawingMatrix.invert(model.inverseDrawingMatrix)
                                    val destination = model.inverseDrawingMatrix.mapPixels(
                                        e1.rawX.px,
                                        e1.rawY.px
                                    )
                                    draggedContent = DraggedContent(
                                        content, PixelCoordinate(
                                            destination.x,
                                            destination.y
                                        ), PixelCoordinate(destination.x, destination.y)
                                    )
                                }
                            }
                            draggedContent?.let {
                                model.dragListener?.invoke(DragEvent.DragStarted(it))
                                invalidate()
                                return true
                            }
                        } else {
                            Log.d(TAG, "action down: out of content bounds")
                        }
                    }
                }
            }

            if (scrollEnabled) {
                if (e1.pointerCount == 1) {
                    isScrollEvent = true
                    if (velocityTracker == null) {
                        velocityTracker = VelocityTracker.obtain()
                    }
                    model.drawingMatrix.postTranslate(-distanceX.px, -distanceY.px)
                    keepPositionInScrollLimit()
                    invalidate()
                    return true
                }
            }

            return false
        }

        override fun onLongPress(event: MotionEvent) {
            Log.d(TAG, "onLongPress() called with: e = [$event]")

            didLongClick = true

            invokeListenerOnContentInsideCoordinates(
                event = event,
                listener = model.longClickListener,
                listenerName = "onLongPress"
            )
        }

        override fun onSingleTapUp(event: MotionEvent): Boolean {
            // if no doubleTapListener registered - call onClick immediately,
            // otherwise wait  for onSingleTapConfirmed will be called.
            if (model.doubleTapListener != null) {
                return false
            }

            didClick = true

            return onClick(event)
        }

        override fun onSingleTapConfirmed(event: MotionEvent): Boolean {
            if (didClick) {
                didClick = false
                return false
            }

            return onClick(event)
        }

        override fun onDoubleTap(firstEvent: MotionEvent): Boolean {
            return invokeListenerOnContentInsideCoordinates(
                event = firstEvent,
                listener = model.doubleTapListener,
                listenerName = "onDoubleTap"
            )
        }
    }

    private fun invokeListenerOnContentInsideCoordinates(
        event: MotionEvent,
        listener: ((PixelCoordinate, List<ContentId>) -> Unit)?,
        listenerName: String,
    ): Boolean {
        if (listener == null) {
            Log.d(TAG, "$listenerName: no listeners")
            return false
        }

        val screenCoordinate = event.toScreenCoordinate()
        val contentCoordinate = getContentCoordinate(screenCoordinate)
        return if (model.contentBounds.contains(contentCoordinate)) {
            listener.invoke(
                contentCoordinate,
                getContentAtCoordinate(
                    contentCoordinate,
                    screenCoordinate
                )
                    .sortedBy { it.options.zOrder }
                    .map { it.contentId }
            )

            true
        } else {
            Log.d(TAG, "$listenerName: out of content bounds")

            false
        }
    }

    private fun getContentAtCoordinate(coordinate: PixelCoordinate, original: PixelCoordinate): List<Content> {
        val clicked = mutableListOf<Content>()
        for (content in model.contentManager.getContent().copyOf()) {
            content.run {
                val contentCoordinate = when {
                    options.resistScale && options.resistRotation ->
                        getContentCoordinateIgnoring(
                            original,
                            content.anchorPoint,
                            ignoreScale = true,
                            ignoreRotation = true
                        )
                    options.resistScale -> getContentCoordinateIgnoring(
                        original,
                        content.anchorPoint,
                        ignoreScale = true
                    )
                    options.resistRotation -> getContentCoordinateIgnoring(
                        original,
                        content.anchorPoint,
                        ignoreRotation = true
                    )
                    else -> coordinate
                }
                if (options.clickable && isClickedOn(contentCoordinate, matrix)) {
                    clicked.add(this)
                }
            }
        }
        return clicked
    }

    private fun normalizeAngle(angle: Float): Float {
        var normalizedAngle = angle % 360.0f
        if (normalizedAngle < 0) {
            normalizedAngle += 360.0f
        }
        return normalizedAngle
    }

    @Synchronized
    private fun keepPositionInScrollLimit() {

        model.contentBounds.insetInto(contentScrollLimit, model.scrollLimitRect)
        model.drawingMatrix.mapRect(model.scrollLimitRect)

        val topDiff = model.scrollLimitRect.bottom
        val bottomDiff = height.px - model.scrollLimitRect.top
        val leftDiff = model.scrollLimitRect.right
        val rightDiff = width.px - model.scrollLimitRect.left

        if (topDiff < 0.px) {
            val xCorrection = if (model.scrollLimitRect.topRight.x < 0.px) model.scrollLimitRect.topRight.x else 0.px
            model.drawingMatrix.postTranslate(xCorrection, -topDiff)
        } else if (bottomDiff < 0.px) {
            model.drawingMatrix.postTranslate(0.px, bottomDiff)
        }

        if (leftDiff < 0.px) {
            model.drawingMatrix.postTranslate(-leftDiff, 0.px)
        } else if (rightDiff < 0.px) {
            model.drawingMatrix.postTranslate(rightDiff, 0.px)
        }
    }

    override fun getViewModelStore(): ViewModelStore {
        return viewModelStoreInstance
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

        coroutineScope.cancel()
        imageRequests.clear()
    }

    fun getContent(): List<Content> {
        return model.contentManager.getContent()
    }

    companion object {
        private const val TAG = "ContentView"

        private val viewModelStoreInstance = ViewModelStore()

        private const val DECELERATE_TIME_BETWEEN_UPDATES_MILLI = 15L
        private const val DECELERATE_ANIMATION_DURATION_MILLI = 300
        private const val DECELERATION_FACTOR = 1.0
        private const val MINIMAL_SCALE_DIFF_THRESHOLD = 0.01
    }
}

internal data class DraggedContent(
    val content: Content,
    val position: PixelCoordinate,
    val originalPosition: PixelCoordinate
)

private data class DecelerationData(
    val count: Int,
    val velocityX: Float,
    val velocityY: Float,
)

data class StatLogConfig(
    val tag: String = "StatLog",
    val count: Boolean = false,
) {
    internal val enabled: Boolean
        get() = count
}

private class StatData(
    var totalContent: Int = 0,
    var shouldDraw: Int = 0,
    var withImage: Int = 0,
    var imageNotProvided: Int = 0,
    var withoutImage: Int = 0,
    var deferred: Int = 0,
    var notLoaded: Int = 0,
) {

    fun reset() {
        totalContent = 0
        shouldDraw = 0
        withImage = 0
        imageNotProvided = 0
        withoutImage = 0
        deferred = 0
        notLoaded = 0
    }

    override fun toString(): String {
        return "StatData(totalContent=$totalContent, shouldDraw=$shouldDraw, withImage=$withImage, imageNotProvided=$imageNotProvided, withoutImage=$withoutImage, deferred=$deferred, notLoaded=$notLoaded)"
    }
}