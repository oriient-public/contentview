package me.oriient.ui.contentview.clustering

import me.oriient.ui.contentview.models.AnchorPoint
import me.oriient.ui.contentview.models.PixelCoordinate
import me.oriient.ui.contentview.models.PixelMatrix
import me.oriient.ui.contentview.models.PixelRect
import me.oriient.ui.contentview.models.PixelSize
import me.oriient.ui.contentview.utils.px
import kotlin.math.abs
import kotlin.math.hypot
import kotlin.math.max

/**
 * Interface that cluster items.
 *
 * @since 1.4.0
 */
interface ClusteringStrategy<T : Clusterable> {
    /**
     * @param items elements to be clustered
     * @param matrix current transformations matrix
     * @return list of clusters with its nested items
     *
     * @since 1.4.0
     */
    fun cluster(items: List<T>, matrix: PixelMatrix): List<List<T>>
}

enum class IntersectionDefinition {
    DIAGONAL,
    MAX_SIDE
}

/**
 * Clustering algorithm based on distance between two items
 *
 * @since 1.4.0
 */
class DistanceBasedClusteringStrategy<T : Clusterable>(
    private val intersectionDefinition: IntersectionDefinition,
) : ClusteringStrategy<T> {

    override fun cluster(items: List<T>, matrix: PixelMatrix): List<List<T>> {
        return greedyClustering(items) { item, cluster ->
            val clusterItem = cluster.firstOrNull() ?: return@greedyClustering false

            val minimumDistance = defineMinimumDistance(
                item = item,
                isItemACluster = cluster.size == 1
            )

            val scaledDistance = minimumDistance / matrix.scaleFactor.value

            val distanceBetweenItems = hypot(
                clusterItem.pixelCoordinate.x.value - item.pixelCoordinate.x.value,
                clusterItem.pixelCoordinate.y.value - item.pixelCoordinate.y.value
            )

            // if distance between items less than defined scaled distance -> create cluster
            distanceBetweenItems <= scaledDistance
        }
    }

    private fun defineMinimumDistance(
        item: Clusterable,
        isItemACluster: Boolean,
    ): Double {
        return when (intersectionDefinition) {
            IntersectionDefinition.DIAGONAL -> {
                if (isItemACluster) {
                    hypot(item.itemSize.width.value.toDouble(), item.itemSize.height.value.toDouble())
                } else {
                    hypot(item.clusterSize.width.value.toDouble(), item.clusterSize.height.value.toDouble())
                }
            }

            IntersectionDefinition.MAX_SIDE -> {
                if (isItemACluster) {
                    max(item.itemSize.width.value.toDouble(), item.itemSize.height.value.toDouble())
                } else {
                    max(item.clusterSize.width.value.toDouble(), item.clusterSize.height.value.toDouble())
                }
            }
        }
    }
}

/**
 *  Clustering algorithm based on intersection between two items
 *
 *  @param itemLocalAnchorPoint - Relative coordinate (0..1f; 0..1f) of the regular item anchor point
 *  to calculate bounds of an item correctly.
 *  Look localAnchorPoint in @see [me.oriient.ui.contentview.models.BillboardParameters].

 *  @param clusterLocalAnchorPoint Relative coordinate (0..1f; 0..1f) of the cluster item anchor point
 *  to calculate bounds of an item correctly.
 *  Look localAnchorPoint in @see [me.oriient.ui.contentview.models.BillboardParameters].
 *
 * @since 1.4.0
 */
class IntersectionClusteringStrategy<T : Clusterable>(
    private val itemLocalAnchorPoint: AnchorPoint,
    private val clusterLocalAnchorPoint: AnchorPoint,
) : ClusteringStrategy<T> {

    override fun cluster(items: List<T>, matrix: PixelMatrix): List<List<T>> {
        return greedyClustering(items) { item, cluster ->
            val clusterItem = cluster.firstOrNull() ?: return@greedyClustering false
            val clusterItemSize = if (cluster.count() == 1) {
                clusterItem.itemSize
            } else {
                clusterItem.clusterSize
            }

            val itemRect = createScaledRectangle(
                item.itemSize,
                item.pixelCoordinate,
                itemLocalAnchorPoint,
                matrix
            )

            val clusterRect = createScaledRectangle(
                clusterItemSize,
                clusterItem.pixelCoordinate,
                clusterLocalAnchorPoint,
                matrix
            )

            // if item intersect cluster -> create cluster
            itemRect.intersect(clusterRect)
        }
    }

    private fun createScaledRectangle(
        size: PixelSize,
        coordinates: PixelCoordinate,
        anchorPoint: AnchorPoint,
        scaleMatrix: PixelMatrix
    ): PixelRect {
        val scaledCoordinates = scaleMatrix.mapCoordinate(coordinates)

        return PixelRect(
            left = (scaledCoordinates.x.value - size.width.value * anchorPoint.x).px,
            top = (scaledCoordinates.y.value - size.height.value * anchorPoint.y).px,
            right = (scaledCoordinates.x.value + size.width.value * abs(1 - anchorPoint.x)).px,
            bottom = (scaledCoordinates.y.value + size.height.value * abs(1 - anchorPoint.y)).px,
        )
    }

    companion object {
        private const val TAG: String = "IntersectionClusteringStrategy"
    }
}

private fun <T : Clusterable> greedyClustering(
    items: List<T>,
    shouldBeClustered: (Clusterable, List<Clusterable>) -> Boolean
): List<List<T>> {
    val clusters = mutableListOf(mutableListOf<T>())

    items.forEach { item ->
        val cluster = clusters.find { shouldBeClustered(item, it) }
        if (cluster == null) {
            clusters.add(mutableListOf(item))
        } else {
            cluster.add(item)
        }
    }

    return clusters
}


