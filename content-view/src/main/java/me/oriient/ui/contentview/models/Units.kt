package me.oriient.ui.contentview.models

import me.oriient.ui.contentview.utils.dg
import me.oriient.ui.contentview.utils.px
import me.oriient.ui.contentview.utils.x

/**
 * This class represents units of screen pixels
 */
@JvmInline
value class Pixel internal constructor(val value: Float): Comparable<Pixel> {

    override fun compareTo(other: Pixel): Int {
        return value.compareTo(other.value)
    }

    operator fun plus(other: Pixel): Pixel {
        return (value + other.value).px
    }

    operator fun minus(other: Pixel): Pixel {
        return (value - other.value).px
    }

    operator fun div(other: Pixel): Float {
        return value / other.value
    }

    operator fun div(other: Int): Pixel {
        return (value / other).px
    }

    internal operator fun div(other: Scale): Pixel {
        return (value / other.value).px
    }

    operator fun times(other: Int): Pixel {
        return (value * other).px
    }

    operator fun times(other: Float): Pixel {
        return (value * other).px
    }

    internal operator fun times(other: Scale): Pixel {
        return (value * other.value).px
    }

    operator fun times(other: Pixel): Pixel {
        return (value * other.value).px
    }

    operator fun unaryMinus(): Pixel {
        return (-value).px
    }

    override fun toString(): String {
        return "Pixel(value=$value)"
    }
}

internal /*value*/ class Scale internal constructor(val value: Float): Comparable<Scale> {

    override fun compareTo(other: Scale): Int {
        return value.compareTo(other.value)
    }

    operator fun times(other: Scale): Scale {
        return (value * other.value).x
    }

    operator fun plus(other: Scale): Scale {
        return (value + other.value).x
    }

    operator fun minus(other: Scale): Scale {
        return (value - other.value).x
    }

    operator fun div(other: Int): Scale {
        return (value / other).x
    }

    operator fun unaryMinus(): Scale {
        return (-value).x
    }

    override fun toString(): String {
        return "Scale(value=$value)"
    }
}

/**
 * This class represents the number of pixels across the width of a view.
 * It's used for representing meaningful and consistent zoom levels across content of different sizes.
 */
@JvmInline
value class PixelsInViewWidth(val value: Float): Comparable<PixelsInViewWidth> {
    override fun compareTo(other: PixelsInViewWidth): Int {
        return this.value.compareTo(other.value)
    }

    override fun toString(): String {
        return "PixelsInViewWidth(value=$value)"
    }
}

internal fun Scale.toPixelsOnViewWidth(viewWidth: Pixel): PixelsInViewWidth {
    if (value < 0.00001) return PixelsInViewWidth(Float.MAX_VALUE)
    return PixelsInViewWidth(viewWidth.value / value)
}

internal fun PixelsInViewWidth.toScale(viewWidth: Pixel): Scale {
    if (value < 0.00001) return Scale(Float.MAX_VALUE)
    return Scale(viewWidth.value / value)
}

/**
 * This class represents rotation of content in degrees.
 */
/*value*/ class Degrees constructor(val value: Float): Comparable<Degrees> {

    override fun compareTo(other: Degrees): Int {
        return value.compareTo(other.value)
    }

    operator fun plus(other: Degrees): Degrees {
        return (value + other.value).dg
    }

    operator fun minus(other: Degrees): Degrees {
        return (value - other.value).dg
    }

    operator fun div(other: Int): Degrees {
        return (value / other).dg
    }

    operator fun unaryMinus(): Degrees {
        return (-value).dg
    }

    companion object {
        fun normalize(angle: Float): Float {
            return when {
                angle > 0f -> 360f - angle
                else -> -angle
            }
        }
    }

    override fun toString(): String {
        return "RotationDegrees(value=$value)"
    }

    operator fun times(i: Int): Degrees {
        return Degrees(value * i)
    }
}

internal operator fun Number.div(scale: Scale): Scale {
    return Scale(this.toFloat() / scale.value)
}