package me.oriient.ui.contentview

import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import me.oriient.ui.contentview.models.Content
import me.oriient.ui.contentview.models.ContentId
import me.oriient.ui.contentview.utils.copyOf


private const val TAG = "ContentManager"

internal class ContentManager {

    private val contentHandler: Handler
    private val content = mutableListOf<Content>()
    // TODO: 12/04/2020 index content by ID?
    private val candidates = mutableListOf<Content>()
    private var drawCandidates = false

    init {
        val receiverThread = HandlerThread(TAG)
        receiverThread.priority = Thread.MAX_PRIORITY
        receiverThread.start()
        val receiverLooper = receiverThread.looper
        contentHandler = Handler(receiverLooper)
    }

    @Synchronized
    fun getContent(): List<Content> {
        return if (drawCandidates) {
            synchronized(candidates) { candidates.copyOf() }
        } else {
            synchronized(content) { content.copyOf() }
        }
    }

    @Synchronized
    fun getContent(id: ContentId): Content? {
        return if (drawCandidates) {
            synchronized(candidates) { candidates.firstOrNull { it.contentId == id } }
        } else {
            synchronized(content) { content.firstOrNull { it.contentId == id } }
        }
    }

    fun setContent(userContent: List<Content>, completion: () -> Unit) {
        Log.d(TAG, "setContent() called with: userContent = ${getContentTypes(userContent)}")
        contentHandler.post {
            synchronized(candidates) {
                candidates.clear()
                candidates.addAll(userContent)
            }
            onNewContentWaiting()
            completion.invoke()
        }
    }

    fun updateContent(userContent: List<Content>, completion: () -> Unit) {
        Log.d(TAG, "setContent() called with: userContent = ${getContentTypes(userContent)}")
        contentHandler.post {
            synchronized(candidates) {
                candidates.clear()
                candidates.addAll(content)
                val userContentIsd = userContent.map { it.contentId }
                candidates.map { candidate ->
                    if (userContentIsd.contains(candidate.contentId)) {
                        userContent.first { it.contentId == candidate.contentId }
                    } else {
                        candidate
                    }
                }
            }
            onNewContentWaiting()
            completion.invoke()
        }
    }

    fun addContent(userContent: List<Content>, completion: () -> Unit) {
        Log.d(TAG, "addContent() called with: userContent = ${getContentTypes(userContent)}")
        contentHandler.post {
            synchronized(candidates) {
                candidates.addAll(content)
                candidates.addAll(userContent)
            }
            onNewContentWaiting()
            completion.invoke()
        }
    }

    fun setOrUpdateContent(userContent: List<Content>, completion: () -> Unit) {
        Log.d(TAG, "setOrUpdateContent() called with: userContent = ${getContentTypes(userContent)}")

        contentHandler.post {
            synchronized(candidates) {
                candidates.clear()
                candidates.addAll(content)
                val userContentIsd = userContent.map { it.contentId }
                candidates.removeAll { userContentIsd.contains(it.contentId) }
                candidates.addAll(userContent)
            }
            onNewContentWaiting()
            completion.invoke()
        }
    }

    fun removeContent(contentIds: List<ContentId>, completion: () -> Unit) {
        Log.d(TAG, "removeContent() called with: contentIds = [$contentIds]")

        contentHandler.post {
            synchronized(candidates) {
                candidates.clear()
                candidates.addAll(content)
                candidates.removeAll { contentIds.contains(it.contentId) }
            }
            onNewContentWaiting()
            completion.invoke()
        }
    }

    fun removeAll(completion: () -> Unit) {
        Log.d(TAG, "removeAll() called")

        contentHandler.post {
            synchronized(candidates) {
                candidates.clear()
            }
            onNewContentWaiting()
            completion.invoke()
        }
    }

    private fun getContentTypes(content: List<Content>): String {
        val sb = StringBuilder("{")
        for (ipsMapDrawable in content) {
            sb.append(ipsMapDrawable::class.java.simpleName).append("(${ipsMapDrawable.contentId.value})").append(", ")
        }
        sb.append("}")
        return sb.toString()
    }

    private fun onNewContentWaiting() {
        synchronized(candidates) { candidates.sort() }
        synchronized(content) {
            drawCandidates = true
            content.clear()
            content.addAll(candidates)
            drawCandidates = false
        }
        synchronized(candidates) { candidates.clear() }
        Log.d(TAG, "onNewContentWaiting: content: ${getContentTypes(content)}")
    }
}