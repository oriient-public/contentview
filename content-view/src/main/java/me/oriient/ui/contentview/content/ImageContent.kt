package me.oriient.ui.contentview.content

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.models.*
import me.oriient.ui.contentview.utils.px

private const val TAG = "ImageContent"

/**
 * Options for scaling the bounds of an actual bitmap to the bounds passed in constructor.
 *
 * @since 1.4.0
 */
enum class ScaleType {

    /**
     * Computes a scale that will maintain the original source aspect ratio, but will also ensure that the source image fits entirely inside the destination image.
     * At least one axis (X or Y) will fit exactly. The result is centered inside the destination image.
     */
    SCALE_TO_FIT,

    /**
     * Applies no transformations on the source image and displays it at its original size.
     */
    NO_SCALE
}

/**
 * A [Content] implementation for drawing an image on a [me.oriient.ui.contentview.ContentView].
 * Note that an image may be the background of the view or drawn as any other element on top of it.
 */
open class ImageContent(
    customContentId: ContentId,
    imageIdentifier: String,
    override var bounds: PixelRect? = null,
    override val anchorPoint: PixelCoordinate = PixelCoordinate(),
    private val scaleType: ScaleType = ScaleType.NO_SCALE,
    configBlock: DrawingOptionsImpl.() -> Unit = {}
) : Content(customContentId, imageIdentifier, configBlock) {

    constructor(
        imageIdentifier: String,
        bounds: PixelRect? = null,
        anchorPoint: PixelCoordinate = PixelCoordinate(),
        scaleType: ScaleType = ScaleType.NO_SCALE,
        configBlock: DrawingOptionsImpl.() -> Unit = {}
    ) : this(ContentId.random, imageIdentifier, bounds, anchorPoint, scaleType, configBlock)

    private val paint = Paint()

    init {
        paint.isAntiAlias = true
    }

    override fun draw(canvas: Canvas, transformMatrix: PixelMatrix, image: Bitmap?) {
        image?.let { bitmap ->
            val bounds = bounds ?: PixelRect(right = bitmap.width.px, bottom = bitmap.height.px).also {
                this.bounds = it
            }

            val imageBounds = when (scaleType) {
                ScaleType.SCALE_TO_FIT -> {
                    performScaleToFit(bitmap, bounds)
                }

                ScaleType.NO_SCALE -> {
                    bounds.rectF
                }
            }

            canvas.drawBitmap(bitmap, null, imageBounds, null)
        }
    }

    override fun isClickedOn(coordinate: PixelCoordinate, transformMatrix: PixelMatrix): Boolean {
        return bounds?.contains(coordinate) ?: false
    }

    override fun move(dx: Pixel, dy: Pixel) {
        bounds?.move(dx, dy)
        anchorPoint.offset(dx, dy)
    }

    override fun toString(): String {
        return "ImageContent(bounds=$bounds, anchorPoint=$anchorPoint)"
    }

    private fun performScaleToFit(bitmap: Bitmap, bounds: PixelRect): RectF {
        val isHorizontal = bitmap.width > bitmap.height
        val widthRatio = bounds.width.value / bitmap.width
        val heightRatio = bounds.height.value / bitmap.height
        val scaleFactor = if (isHorizontal) widthRatio else heightRatio
        val scaledWidth = bitmap.width * scaleFactor
        val scaledHeight = bitmap.height * scaleFactor

        val scaledBounds = RectF(
            bounds.left.value,
            bounds.top.value,
            bounds.left.value + scaledWidth,
            bounds.top.value + scaledHeight
        )

        if (widthRatio != heightRatio) {
            val offsetX = (bounds.width.value - scaledWidth) / 2
            val offsetY = (bounds.height.value - scaledHeight) / 2
            scaledBounds.offset(offsetX, offsetY)
        }

        return scaledBounds
    }

}

private operator fun Int.div(scale: Scale): Scale {
    return Scale(this / scale.value)
}
