package me.oriient.ui.contentview

import me.oriient.ui.contentview.models.Content
import me.oriient.ui.contentview.models.PixelsInViewWidth

/**
 * General set of instructions for [ContentView] to describe how a certain [Content] should behave on the view.
 */

interface DrawingOptions {
    /**
     * Indicates which [Content] should be visible when two or more elements occupy the same coordinates.
     * Use this parameter for grouping elements by type and [zPriority] for ordering within each group.
     */
    val zOrder: Int

    /**
     * Indicates which [Content] should be visible when two or more elements with the same [zOrder] occupy the same coordinates.
     * Note: [zOrder] takes priority over [zPriority].
     */
    val zPriority: Int

    /**
     * The minimal zoom level at which this [Content] will be visible on the [ContentView].
     */
    val minZoom: PixelsInViewWidth

    /**
     * The maximal zoom level at which this [Content] will be visible on the [ContentView].
     */
    val maxZoom: PixelsInViewWidth

    /**
     * The minimal zoom level at which this [Content] will resist scaling if [resistScale] is set to `true`.
     */
    val minResistScale: PixelsInViewWidth

    /**
     * The maximal zoom level at which this [Content] will resist scaling if [resistScale] is set to `true`.
     */
    val maxResistScale: PixelsInViewWidth

    /**
     * If set to `true`, the [Content] will not rotate along with the [ContentView].
     */
    val resistRotation: Boolean

    /**
     * If set to `true`, the [Content] will not scale along with the [ContentView].
     */
    val resistScale: Boolean

    /**
     * If set to `true`, the [Content] will be able to receive click and long click events. @see [ContentView.setClickListener].
     */
    val clickable: Boolean

    /**
     * If set to `true`, the [Content] will be draggable across the [ContentView] within the bounds of the background.
     * @see [ContentView.setDragListener].
     */
    val draggable: Boolean
}

/**
 * Implementation of the [DrawingOptions] interface for configuration porpoises.
 */
data class DrawingOptionsImpl(
    override var zOrder: Int = 0,
    override var zPriority: Int = 0,
    override var minZoom: PixelsInViewWidth = PixelsInViewWidth(0f),
    override var maxZoom: PixelsInViewWidth = PixelsInViewWidth(Float.MAX_VALUE),
    override var maxResistScale: PixelsInViewWidth = PixelsInViewWidth(Float.MAX_VALUE),
    override var minResistScale: PixelsInViewWidth = PixelsInViewWidth(0f),
    override var resistRotation: Boolean = false,
    override var resistScale: Boolean = false,
    override var clickable: Boolean = false,
    override var draggable: Boolean = false,
): DrawingOptions {

    constructor(options: DrawingOptions): this(
        options.zOrder,
        options.zPriority,
        options.minZoom,
        options.maxZoom,
        options.maxResistScale,
        options.minResistScale,
        options.resistRotation,
        options.resistScale,
        options.clickable,
        options.draggable,
    )

    override fun toString(): String {
        return "DrawingOptions(zOrder=$zOrder, zPriority=$zPriority, minZoom=$minZoom, maxZoom=$maxZoom, maxResistScale=$maxResistScale, minResistScale=$minResistScale, resistRotation=$resistRotation, resistScale=$resistScale, clickable=$clickable, draggable=$draggable)"
    }
}