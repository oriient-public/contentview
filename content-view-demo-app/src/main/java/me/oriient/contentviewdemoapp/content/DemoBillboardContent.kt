package me.oriient.contentviewdemoapp.content

import android.graphics.Bitmap
import me.oriient.ui.contentview.DrawingOptionsImpl
import me.oriient.ui.contentview.clustering.Clusterable
import me.oriient.ui.contentview.content.BillboardContent
import me.oriient.ui.contentview.models.BillboardParameters
import me.oriient.ui.contentview.models.ContentId
import me.oriient.ui.contentview.models.PixelCoordinate
import me.oriient.ui.contentview.models.PixelSize

class DemoBillboardContent(
    customContentId: ContentId,
    override val pixelCoordinate: PixelCoordinate,
    override val itemSize: PixelSize,
    override val clusterSize: PixelSize,
    imageIdentifier: String?,
    imageOverlay: Bitmap? = null,
    val billboardParameters: BillboardParameters = BillboardParameters(),
    configBlock: DrawingOptionsImpl.() -> Unit = {}
) : BillboardContent(customContentId, pixelCoordinate, imageIdentifier, imageOverlay, billboardParameters, configBlock),
    Clusterable