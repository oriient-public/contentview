package me.oriient.contentviewdemoapp

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import me.oriient.contentviewdemoapp.content.DemoBillboardContent
import me.oriient.contentviewdemoapp.databinding.FragmentFirstBinding
import me.oriient.ui.contentview.clustering.ClusterManager
import me.oriient.ui.contentview.clustering.ClusterManagerImpl
import me.oriient.ui.contentview.clustering.DistanceBasedClusteringStrategy
import me.oriient.ui.contentview.clustering.IntersectionClusteringStrategy
import me.oriient.ui.contentview.clustering.IntersectionDefinition
import me.oriient.ui.contentview.content.ImageContent
import me.oriient.ui.contentview.content.PathContent
import me.oriient.ui.contentview.content.PolygonContent
import me.oriient.ui.contentview.content.TextContent
import me.oriient.ui.contentview.models.AnimationMode
import me.oriient.ui.contentview.models.BillboardParameters
import me.oriient.ui.contentview.models.Content
import me.oriient.ui.contentview.models.ContentColor
import me.oriient.ui.contentview.models.ContentId
import me.oriient.ui.contentview.models.Degrees
import me.oriient.ui.contentview.models.PixelCoordinate
import me.oriient.ui.contentview.models.PixelRect
import me.oriient.ui.contentview.models.PixelSize
import me.oriient.ui.contentview.models.PixelsInViewWidth
import me.oriient.ui.contentview.utils.px
import kotlin.concurrent.fixedRateTimer

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var myImagesProvider: MyImagesProvider
    private lateinit var clusterManager: ClusterManager<DemoBillboardContent>
    private var isDisplayingMap = false
    private var mapFocusAngles =
        listOf(0f, 90f, 0f, 0f, 0f, 90f, 0f, 90f, 180f, 90f, 40f, 30f, 20f, 10f, 0f, 0f, 0f, 270f)
    private var mapFocusAngleNumber = 0

    private val billboardParameters = BillboardParameters()
    private val clusterParameters = BillboardParameters(color = clusterColor)


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myImagesProvider = MyImagesProvider(requireContext())

        setupClustering()

        binding.contentView.viewTreeObserver.addOnGlobalLayoutListener {
            if (!isDisplayingMap) {
                isDisplayingMap = true
                displayMap()
                displayColorado()
                displayWyoming()
                displayLosAngelesToNewYork()
            }
        }

        binding.centerButton.setOnClickListener {
            fixedRateTimer(
                period = 150,
                action = {
                    fitMapToView()
                }
            )

        }
    }

    private fun setupClustering() {
        val intersectionClusteringStrategy = IntersectionClusteringStrategy<DemoBillboardContent>(
            itemLocalAnchorPoint = billboardParameters.localAnchorPoint,
            clusterLocalAnchorPoint = clusterParameters.localAnchorPoint
        )

        val distanceBasedClusteringStrategy = DistanceBasedClusteringStrategy<DemoBillboardContent>(
            intersectionDefinition = IntersectionDefinition.DIAGONAL,
        )

        clusterManager = ClusterManagerImpl(
            clusteringStrategy = intersectionClusteringStrategy,
            singleItemProvider = { item -> displayBillboard(item) },
            clusterItemProvider = { items -> displayCluster(items) }
        )

        clusterManager.setItems(getBillboards())
        clusterManager.attachMapView(binding.contentView)
    }

    private fun displayBillboard(billboardContent: DemoBillboardContent): List<Content> {
        return listOf(DemoBillboardContent(
            pixelCoordinate = billboardContent.pixelCoordinate,
            imageIdentifier = billboardContent.imageIdentifier,
            billboardParameters = billboardContent.billboardParameters,
            customContentId = billboardContent.contentId,
            clusterSize = billboardContent.clusterSize,
            itemSize = billboardContent.itemSize,
        ) {
            zOrder = DisplayOrder.BILLBOARD.ordinal
            resistRotation = true
            resistScale = true
        })
    }

    private fun displayCluster(billboards: List<DemoBillboardContent>): List<Content> {
        val firstBillboard = billboards.first()
        return listOf(DemoBillboardContent(
            pixelCoordinate = firstBillboard.pixelCoordinate,
            imageIdentifier = firstBillboard.imageIdentifier,
            billboardParameters = BillboardParameters(
                width = firstBillboard.clusterSize.width,
                height = firstBillboard.clusterSize.height,
                color = clusterColor
            ),
            customContentId = firstBillboard.contentId,
            itemSize = firstBillboard.itemSize,
            clusterSize = firstBillboard.clusterSize
        ) {
            zOrder = DisplayOrder.BILLBOARD.ordinal
            resistRotation = true
            resistScale = true
        })
    }

    private fun displayMap() {
        binding.contentView.imageProvider = myImagesProvider

        binding.contentView.setClickListener { pixelCoordinate, contentIds ->
            Log.d(TAG, "displayMap: onClick() ${System.currentTimeMillis()}")
        }

        binding.contentView.setDoubleTapListener { pixelCoordinate, contentIds ->
            Log.d(TAG, "displayMap: onDoubleTap()")
        }

        binding.contentView.setBackground(
            ImageContent(
                MyImagesProvider.MAP_IMAGE_ID,
                PixelRect(
                    left = 0.px,
                    top = 0.px,
                    right = myImagesProvider.mapWidthDp.px,
                    bottom = myImagesProvider.mapHeightDp.px,
                )
            ) {
                zOrder = DisplayOrder.MAP.ordinal
            }
        )
    }

    private fun fitMapToView() {
        this.activity?.runOnUiThread {
            binding.contentView.centerOnPixels(
                x = (myImagesProvider.mapWidthDp / 2).px,
                y = (myImagesProvider.mapHeightDp / 2).px,
                scale = PixelsInViewWidth(myImagesProvider.mapWidthDp),
                rotation = Degrees(mapFocusAngles[mapFocusAngleNumber % mapFocusAngles.size]),
                animationDuration = 1000,
                animationMode = AnimationMode.Linear
            )

            mapFocusAngleNumber++
//            rotationAngle += 20.0f
        }
    }

    private fun displayColorado() {
        val bottomLeft = PixelCoordinate(196.px, 167.px).scaleToScreen(requireContext())
        val bottomRight = PixelCoordinate(258.px, 171.px).scaleToScreen(requireContext())
        val topRight = PixelCoordinate(261.px, 126.px).scaleToScreen(requireContext())
        val topLeft = PixelCoordinate(201.px, 121.px).scaleToScreen(requireContext())

        val center = PixelCoordinate(
            topLeft.x + ((bottomRight.x - topLeft.x) / 2),
            topRight.y + ((bottomLeft.y - topRight.y) / 2),
        )

        binding.contentView.addContent(
            listOf(
                PolygonContent(
                    listOf(
                        bottomLeft,
                        bottomRight,
                        topRight,
                        topLeft,
                    ),
                    ContentColor.Int(Color.BLUE),
                ) {
                    zOrder = DisplayOrder.POLYGON.ordinal
                },
                TextContent(
                    coordinate = center,
                    fontSize = textSize,
                    text = listOf(getString(R.string.colorado)),
                    color = textColor,
                    anchorPoint = center,
                    borderColor = textBorderColor,
                    strokeWidth = textBorderWidth,
                ) {
                    zOrder = DisplayOrder.TEXT.ordinal
                    resistRotation = true
                    resistScale = true
                },
            )
        )
    }

    private fun displayWyoming() {

        val mapWidthInKilometers = 5344
        val kilometersInPixel = mapWidthInKilometers / myImagesProvider.mapWidthPx

        val bottomLeft = PixelCoordinate(
            (1772 / kilometersInPixel).px,
            (1955 / kilometersInPixel).px,
        ).scaleToScreen(requireContext())

        val bottomRight = PixelCoordinate(
            (2350 / kilometersInPixel).px,
            (1879 / kilometersInPixel).px,
        ).scaleToScreen(requireContext())

        val topRight = PixelCoordinate(
            (2378 / kilometersInPixel).px,
            (2378 / kilometersInPixel).px,
        ).scaleToScreen(requireContext())

        val topLeft = PixelCoordinate(
            (1839 / kilometersInPixel).px,
            (2399 / kilometersInPixel).px,
        ).scaleToScreen(requireContext())

        bottomLeft.updateY(myImagesProvider.mapHeightDp.px - bottomLeft.y)
        bottomRight.updateY(myImagesProvider.mapHeightDp.px - bottomRight.y)
        topRight.updateY(myImagesProvider.mapHeightDp.px - topRight.y)
        topLeft.updateY(myImagesProvider.mapHeightDp.px - topLeft.y)

        val center = PixelCoordinate(
            topLeft.x + ((bottomRight.x - topLeft.x) / 2),
            topRight.y + ((bottomLeft.y - topRight.y) / 2),
        )

        binding.contentView.addContent(
            listOf(
                PolygonContent(
                    listOf(
                        bottomLeft,
                        bottomRight,
                        topRight,
                        topLeft,
                    ),
                    ContentColor.Int(Color.RED),
                ) {
                    zOrder = DisplayOrder.POLYGON.ordinal
                },
                TextContent(
                    coordinate = center,
                    fontSize = textSize,
                    text = listOf(getString(R.string.wyoming)),
                    color = textColor,
                    anchorPoint = center,
                    borderColor = textBorderColor,
                    strokeWidth = textBorderWidth,
                ) {
                    zOrder = DisplayOrder.TEXT.ordinal
                    resistRotation = true
                    resistScale = true
                },
            )
        )
    }

    private fun getBillboards(): List<DemoBillboardContent> {
        val itemSize = PixelSize(
            width = billboardParameters.width,
            height = billboardParameters.height + billboardParameters.bottomStandHeight,
        )

        val itemSizeBig = PixelSize(
            width = billboardParameters.width * 2,
            height = billboardParameters.height + billboardParameters.bottomStandHeight,
        )

        val clusterSize = PixelSize(
            width = clusterWidth,
            height = clusterParameters.height + billboardParameters.bottomStandHeight
        )

        val clusterSizeBig = PixelSize(
            width = clusterWidth * 2,
            height = clusterParameters.height + billboardParameters.bottomStandHeight
        )

        return listOf(
            DemoBillboardContent(
                customContentId = ContentId("somethingCloseToLosAngeles"),
                pixelCoordinate = closeToLosAngelesCoordinate.scaleToScreen(requireContext()),
                imageIdentifier = MyImagesProvider.LOS_ANGELES_IMAGE_ID,
                billboardParameters = BillboardParameters(
                    color = ContentColor.Int(Color.YELLOW),
                    width = itemSizeBig.width,
                    height = itemSizeBig.height
                ),
                itemSize = itemSizeBig,
                clusterSize = clusterSizeBig
            ) {
                zOrder = DisplayOrder.BILLBOARD.ordinal
                resistRotation = true
                resistScale = true
            },
            DemoBillboardContent(
                customContentId = ContentId("LosAngeles"),
                pixelCoordinate = losAngelesCoordinate.scaleToScreen(requireContext()),
                imageIdentifier = MyImagesProvider.LOS_ANGELES_IMAGE_ID,
                billboardParameters = BillboardParameters(
                    color = ContentColor.Int(Color.YELLOW),
                    width = itemSize.width,
                    height = itemSize.height
                ),
                itemSize = itemSize,
                clusterSize = clusterSize
            ) {
                zOrder = DisplayOrder.BILLBOARD.ordinal
                resistRotation = true
                resistScale = true
            },
            DemoBillboardContent(
                customContentId = ContentId("newYork"),
                pixelCoordinate = newYorkCoordinate.scaleToScreen(requireContext()),
                imageIdentifier = MyImagesProvider.NEW_YORK_IMAGE_ID,
                billboardParameters = BillboardParameters(
                    color = ContentColor.Int(Color.YELLOW),
                    width = itemSize.width,
                    height = itemSize.height
                ),
                itemSize = itemSize,
                clusterSize = clusterSize
            ) {
                zOrder = DisplayOrder.BILLBOARD.ordinal
                resistRotation = true
                resistScale = true
            }
        )
    }

    private fun displayLosAngelesToNewYork() {
        binding.contentView.addContent(listOf(
            PathContent(
                listOf(
                    losAngelesCoordinate.scaleToScreen(requireContext()),
                    newYorkCoordinate.scaleToScreen(requireContext()),
                ),
                ContentColor.Int(Color.MAGENTA),
                10.px,
                keepWidth = true,
            ) {
                zOrder = DisplayOrder.PATH.ordinal
            }
        ))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        clusterManager.detachMapView()
        _binding = null
    }

    companion object {
        private val TAG = FirstFragment::class.simpleName

        private val textSize = 27.px
        private val textColor = ContentColor.Int(Color.DKGRAY)
        private val textBorderWidth = 0.3.px
        private val textBorderColor = ContentColor.Int(Color.WHITE)

        private val losAngelesCoordinate = PixelCoordinate(111.px, 190.px)
        private val closeToLosAngelesCoordinate = PixelCoordinate(140.px, 160.px)
        private val newYorkCoordinate = PixelCoordinate(501.px, 115.px)

        private val clusterWidth = 140.px
        private val clusterColor = ContentColor.Int(Color.RED)
    }
}