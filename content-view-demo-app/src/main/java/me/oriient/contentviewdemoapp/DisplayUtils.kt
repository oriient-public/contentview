package me.oriient.contentviewdemoapp

import android.content.Context
import me.oriient.ui.contentview.models.PixelCoordinate

fun getScreenScale(context: Context): Float {
    return context.resources.displayMetrics.density
}

fun PixelCoordinate.scaleToScreen(context: Context): PixelCoordinate {
    val screenScale = getScreenScale(context)
    return PixelCoordinate(
        x * screenScale,
        y * screenScale,
    )
}


enum class DisplayOrder {
    MAP, POLYGON, PATH, TEXT, BILLBOARD
}