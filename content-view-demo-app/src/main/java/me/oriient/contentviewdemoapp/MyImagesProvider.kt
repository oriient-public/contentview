package me.oriient.contentviewdemoapp

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import coil.imageLoader
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.oriient.ui.contentview.ImageProvider
import me.oriient.ui.contentview.ImageRequest
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class MyImagesProvider(context: Context): ImageProvider {

    private val map: Bitmap = drawableToBitmap(getResourceDrawable(context, R.drawable.usa_map))!!
    private val losAngeles: Bitmap = drawableToBitmap(getResourceDrawable(context, R.drawable.hollywood))!!
    private val newYork: Bitmap = drawableToBitmap(getResourceDrawable(context, R.drawable.ic_statue_of_liberty))!!

    // scaled size in dp. equals original size times screen scale
    val mapWidthDp = map.width.toFloat()
    // scaled size in dp. equals original size times screen scale
    val mapHeightDp = map.height.toFloat()
    // original image resource size
    val mapWidthPx = map.width / getScreenScale(context)
    // original image resource size
    val mapHeightPx = map.height / getScreenScale(context)

    private val myCache = mutableMapOf<String, Bitmap>()
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    val imageLoader = context.imageLoader

    override fun provideImage(context: Context, imageIdentifier: String): ImageRequest? {

        myCache[imageIdentifier]?.let {
            return ImageRequest.Bitmap(it)
        }

        return when (imageIdentifier) {
            MAP_IMAGE_ID -> {
                asyncLoadResource(context, R.drawable.usa_map, MAP_IMAGE_ID)
            }
            LOS_ANGELES_IMAGE_ID -> {
                asyncLoadResource(context, R.drawable.hollywood, LOS_ANGELES_IMAGE_ID)
            }
            NEW_YORK_IMAGE_ID -> {
                asyncLoadResource(context, R.drawable.ic_statue_of_liberty, NEW_YORK_IMAGE_ID)
            }
            else -> null
        }
    }

    private fun asyncLoadResource(
        context: Context,
        @DrawableRes
        res: Int,
        imageIdentifier: String,
    ) = ImageRequest.Deferred {
        suspendCoroutine { continuation ->
            val asyncRequest = coil.request.ImageRequest.Builder(context)
                .data(res)
                .target(
                    onSuccess = {
                        myCache[imageIdentifier] = it.toBitmap()
                        continuation.resume(true)
                    },
                    onError = {
                        continuation.resume(false)
                    },
                ).build()
            coroutineScope.launch {
                imageLoader.execute(asyncRequest)
            }
        }
    }

    companion object {
        private val TAG = MyImagesProvider::class.simpleName

        const val MAP_IMAGE_ID = "mapImage"
        const val LOS_ANGELES_IMAGE_ID = "losAngelesImage"
        const val NEW_YORK_IMAGE_ID = "newYorkImage"
    }
}

private fun drawableToBitmap(drawable: Drawable?, width: Int = drawable?.intrinsicWidth ?: 0, height: Int = drawable?.intrinsicHeight ?: 0): Bitmap? {

    drawable ?: return null

    if (drawable is BitmapDrawable) {
        return drawable.bitmap
    }
    val bitmap = Bitmap.createBitmap(
        width,
        height,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    drawable.setBounds(0, 0, canvas.width, canvas.height)
    drawable.draw(canvas)
    return bitmap
}

private fun getResourceDrawable(context: Context, @DrawableRes resId: Int): Drawable? {
    return ResourcesCompat.getDrawable(context.resources, resId, context.theme)
}