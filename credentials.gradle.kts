val props = java.util.Properties()
props.load(java.io.FileInputStream("$rootDir/local.properties"))

extra["publicUrl"] = props.getProperty("artifactory.public.url")
extra["publicUsername"] = props.getProperty("artifactory.public.username")
extra["publicPassword"] = props.getProperty("artifactory.public.password")

extra["privateUrl"] = props.getProperty("artifactory.private.url")
extra["privateUsername"] = props.getProperty("artifactory.private.username")
extra["privatePassword"] = props.getProperty("artifactory.private.password")