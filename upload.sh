./gradlew :content-view:clean

compileResult=$?
if [ ${compileResult} -ne 0 ]; then
	echo ">>> content-view clean failed!!!"
	exit 1
fi

if [ "$1" == release ];
    then
      echo ">>> assembling release"
      ./gradlew :content-view:assembleRelease
    else
      echo ">>> assembling debug"
      ./gradlew :content-view:assembleDebug
fi

compileResult=$?
if [ ${compileResult} -ne 0 ]; then
	echo ">>> content-view assemble failed!!!"
	exit 2
fi

./gradlew :content-view:artifactoryPublish

compileResult=$?
if [ ${compileResult} -ne 0 ]; then
	echo ">>> content-view publish failed!!!"
	exit 3
fi

echo ">>> Upload success"