// consts
val debug = "debug"
val rc = "rc"
val release = "release"

// configs
extra["versionName"] = "1.5.0-1144.1"
extra["versionCode"] = 8
extra["buildType"] = debug
